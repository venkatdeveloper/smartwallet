﻿using Xamarin.Forms.Platform.Android;
using Android.Content;
using Xamarin.Forms;
using SmartWallet.Renderer;
using SmartWallet.Droid.Renderer;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(ExtendedEditorRenderer))]
namespace SmartWallet.Droid.Renderer
{
    public class ExtendedEditorRenderer : EditorRenderer
    {
        public ExtendedEditorRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Control == null) return;

            Control.Background = null;
            Control.SetPadding(0, 0, 0, 0);
        }
    }
}