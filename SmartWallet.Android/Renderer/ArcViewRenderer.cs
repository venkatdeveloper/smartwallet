﻿using Android.Content;
using Android.Graphics;
using SmartWallet.Droid.Renderer;
using SmartWallet.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Support.V4.Content;

[assembly: ExportRenderer(typeof(ArcView), typeof(ArcViewRenderer))]
namespace SmartWallet.Droid.Renderer
{
    public class ArcViewRenderer : BoxRenderer
    {
        readonly Paint paint;
        public ArcViewRenderer(Context context) : base(context)
        {
            paint = new Paint();
        }

        override public void Draw(Canvas canvas)
        {
            base.Draw(canvas);

            Path path = new Path();
            var t = ContextCompat.GetColor(Context, Resource.Color.colorPrimary);
            paint.Color = new Android.Graphics.Color(ContextCompat.GetColor(Context, Resource.Color.colorPrimary));
            path.MoveTo(0, 0);
            path.LineTo(Width, 0);
            path.LineTo(Width, Height / 2);
            path.QuadTo(Width / 2, Height, 0, Height / 2);
            path.Close();
            canvas.DrawPath(path, paint);
        }
    }
}