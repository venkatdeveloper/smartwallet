﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;

using SmartWallet.Droid.Renderer;
using SmartWallet.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using FrameRenderer = Xamarin.Forms.Platform.Android.AppCompat.FrameRenderer;

[assembly: ExportRenderer(typeof(CorneredImage), typeof(RoundedImage))]
namespace SmartWallet.Droid.Renderer
{
    public class RoundedImage : ImageRenderer
    {
        public RoundedImage(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement != null && Control != null)
            {
                UpdateCornerRadius();
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(CorneredImage.CornerRadius) ||
            e.PropertyName == nameof(CorneredImage))
            {
                UpdateCornerRadius();
            }
        }

        private void UpdateCornerRadius()
        {
            if (Control.Background is GradientDrawable backgroundGradient)
            {
                var cornerRadius = (Element as CorneredImage)?.CornerRadius;
                if (!cornerRadius.HasValue)
                {
                    return;
                }

                var topLeftCorner = Context.ToPixels(cornerRadius.Value.TopLeft);
                var topRightCorner = Context.ToPixels(cornerRadius.Value.TopRight);
                var bottomLeftCorner = Context.ToPixels(cornerRadius.Value.BottomLeft);
                var bottomRightCorner = Context.ToPixels(cornerRadius.Value.BottomRight);

                var cornerRadii = new[]
                {
                       topLeftCorner,
topLeftCorner,

topRightCorner,
topRightCorner,

bottomRightCorner,
bottomRightCorner,

bottomLeftCorner,
bottomLeftCorner,
};

                backgroundGradient.SetCornerRadii(cornerRadii);
            }
        }

    }
}
