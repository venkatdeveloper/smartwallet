﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using SmartWallet.Droid.Renderer;
using SmartWallet.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(GradientFrame), typeof(GradientFrameRenderer))]
namespace SmartWallet.Droid.Renderer
{
    public class GradientFrameRenderer: Xamarin.Forms.Platform.Android.AppCompat.FrameRenderer
    {
        GradientDrawable layer1;
        GradientDrawable layer2;
        GradientFrame formsElement;

        public GradientFrameRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);
            if (Element == null)
                return;
            formsElement = Element as GradientFrame;
            DrawShape();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == GradientFrame.IsVisibleGradientProperty.PropertyName)
            {
                DrawShape();
            }
            else if (e.PropertyName == GradientFrame.IsBorderVisibleProprty.PropertyName)
            {
                DrawShape();
            }
        }

        void DrawShape()
        {
            var borderStatingColor = formsElement.StartColor.ToAndroid();
            var borderEndingColor = formsElement.EndColor.ToAndroid();
            var fillStartingColor = formsElement.StartColor.ToAndroid();
            var fillEndingColor = formsElement.EndColor.ToAndroid();

            if (!formsElement.IsBorderVisible)
            {
                borderStatingColor = Android.Graphics.Color.Transparent;
                borderEndingColor = Android.Graphics.Color.Transparent;
            }
            if (!formsElement.IsVisibleGradient)
            {
                fillStartingColor = formsElement.BackgroundColor.ToAndroid(); //Android.Graphics.Color.ParseColor("#eceded");
                fillEndingColor = formsElement.BackgroundColor.ToAndroid();//Android.Graphics.Color.ParseColor("#eceded");
            }

            layer1 = new GradientDrawable(GradientDrawable.Orientation.BlTr, new int[] { borderStatingColor, borderEndingColor });
            layer2 = new GradientDrawable(GradientDrawable.Orientation.BlTr, new int[] { fillStartingColor, fillEndingColor });
            layer1.SetCornerRadius(Context.ToPixels(formsElement.CornerRadius));
            layer2.SetCornerRadius(Context.ToPixels(formsElement.CornerRadius - 1));
            //layer2.SetColor(formsElement.BackgroundColor.ToAndroid());
            LayerDrawable layerDrawable = new LayerDrawable(new Drawable[] {
                layer1, layer2
            });
            var borderWidth = formsElement.BorderWidth;
            layerDrawable.SetLayerInset(1, (int)Context.ToPixels(borderWidth), (int)Context.ToPixels(borderWidth), (int)Context.ToPixels(borderWidth), (int)Context.ToPixels(borderWidth));
            Background = layerDrawable;
        }
    }
}

