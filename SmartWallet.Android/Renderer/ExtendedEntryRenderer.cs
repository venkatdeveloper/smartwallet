﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using SmartWallet.Droid.Renderer;
using SmartWallet.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderLessEntry), typeof(ExtendedEntryRenderer))]
namespace SmartWallet.Droid.Renderer
{
    public class ExtendedEntryRenderer : EntryRenderer
    {
        GradientDrawable backgroundDrawable;
        BorderLessEntry formsElement;

        bool disposed;

        public ExtendedEntryRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
                return;
            formsElement = Element as BorderLessEntry;
            Control.Background = null;

            UpdateBackgroundColor();
            UpdateCornerRadius();
            UpdateGravity();
            UpdatePadding();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Entry.HorizontalTextAlignmentProperty.PropertyName == e.PropertyName)
            {
                UpdateGravity();
            }
        }

        protected void UpdateGravity()
        {
            Control.Gravity = Android.Views.GravityFlags.CenterVertical | ToAndroid(Element.HorizontalTextAlignment);
        }

        protected override void UpdateBackgroundColor()
        {
            if (backgroundDrawable != null)
            {
                var backgroundColor = formsElement.BackgroundColor;
                if (backgroundColor != Color.Default)
                    backgroundDrawable.SetColor(backgroundColor.ToAndroid());
                else
                    backgroundDrawable.SetColor(backgroundColor.ToAndroid(Color.Transparent));
                this.SetBackground(backgroundDrawable);
            }
        }

        protected void UpdateCornerRadius()
        {
            var cornerRadius = formsElement.CornerRadius;
            if (cornerRadius == new CornerRadius(0d))
            {
                backgroundDrawable?.Dispose();
                backgroundDrawable = null;
            }
            else
            {
                this.SetBackground(backgroundDrawable = new GradientDrawable());
                if (Background is GradientDrawable backgroundGradient)
                {
                    var cornerRadii = new[] {
                        Context.ToPixels(cornerRadius.TopLeft),
                        Context.ToPixels(cornerRadius.TopLeft),

                        Context.ToPixels(cornerRadius.TopRight),
                        Context.ToPixels(cornerRadius.TopRight),

                        Context.ToPixels(cornerRadius.BottomRight),
                        Context.ToPixels(cornerRadius.BottomRight),

                        Context.ToPixels(cornerRadius.BottomLeft),
                        Context.ToPixels(cornerRadius.BottomLeft)
                    };

                    backgroundGradient.SetCornerRadii(cornerRadii);
                }
            }
            UpdateBackgroundColor();
        }

        protected void UpdatePadding()
        {
            var left = (int)Context.ToPixels(formsElement.Padding.Left);
            var right = (int)Context.ToPixels(formsElement.Padding.Right);
            var top = (int)Context.ToPixels(formsElement.Padding.Top);
            var bottom = (int)Context.ToPixels(formsElement.Padding.Bottom);
            Control.SetPadding(left, top, right, bottom);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed) return;

            disposed = true;
            if (disposing)
            {
                if (backgroundDrawable != null)
                {
                    backgroundDrawable.Dispose();
                    backgroundDrawable = null;
                }
            }
            base.Dispose(disposing);
        }

        Android.Views.GravityFlags ToAndroid(TextAlignment textAlignment)
        {
            var alignment = Android.Views.GravityFlags.Start;
            switch (textAlignment)
            {
                case Xamarin.Forms.TextAlignment.Start:
                    break;
                case Xamarin.Forms.TextAlignment.Center:
                    alignment = Android.Views.GravityFlags.CenterHorizontal;
                    break;
                case Xamarin.Forms.TextAlignment.End:
                    alignment = Android.Views.GravityFlags.End;
                    break;
            }
            return alignment;
        }
    }
}