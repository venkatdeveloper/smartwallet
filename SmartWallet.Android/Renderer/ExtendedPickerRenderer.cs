﻿using Android.Content;
using Android.Graphics.Drawables;
using SmartWallet.Droid.Renderer;
using SmartWallet.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomPicker), typeof(ExtendedPickerRenderer))]
namespace SmartWallet.Droid.Renderer
{
    public class ExtendedPickerRenderer : Xamarin.Forms.Platform.Android.AppCompat.PickerRenderer
    {
        //GradientDrawable backgroundDrawable;
        //CustomPicker formsElement;
        //bool disposed;

        public ExtendedPickerRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;
           // formsElement = Element as CustomPicker;
            Control.Background = null;

            //UpdateBackgroundColor();
            //UpdateCornerRadius();
            //UpdatePadding();
        }

        //protected override void UpdateBackgroundColor()
        //{
        //    if (backgroundDrawable != null)
        //    {
        //        var backgroundColor = formsElement.BackgroundColor;
        //        if (backgroundColor != Color.Default)
        //            backgroundDrawable.SetColor(backgroundColor.ToAndroid());
        //        else
        //            backgroundDrawable.SetColor(backgroundColor.ToAndroid(Color.Transparent));
        //        this.SetBackground(backgroundDrawable);
        //    }
        //}

        //protected void UpdateCornerRadius()
        //{
        //    var cornerRadius = formsElement.CornerRadius;
        //    if (cornerRadius == new CornerRadius(0d))
        //    {
        //        backgroundDrawable?.Dispose();
        //        backgroundDrawable = null;
        //    }
        //    else
        //    {
        //        this.SetBackground(backgroundDrawable = new GradientDrawable());
        //        if (Background is GradientDrawable backgroundGradient)
        //        {
        //            var cornerRadii = new[] {
        //                Context.ToPixels(cornerRadius.TopLeft),
        //                Context.ToPixels(cornerRadius.TopLeft),

        //                Context.ToPixels(cornerRadius.TopRight),
        //                Context.ToPixels(cornerRadius.TopRight),

        //                Context.ToPixels(cornerRadius.BottomRight),
        //                Context.ToPixels(cornerRadius.BottomRight),

        //                Context.ToPixels(cornerRadius.BottomLeft),
        //                Context.ToPixels(cornerRadius.BottomLeft)
        //            };

        //            backgroundGradient.SetCornerRadii(cornerRadii);
        //        }
        //    }
        //    UpdateBackgroundColor();
        //}

        //protected void UpdatePadding()
        //{
        //    var left = (int)Context.ToPixels(formsElement.Padding.Left);
        //    var right = (int)Context.ToPixels(formsElement.Padding.Right);
        //    var top = (int)Context.ToPixels(formsElement.Padding.Top);
        //    var bottom = (int)Context.ToPixels(formsElement.Padding.Bottom);
        //    Control.SetPadding(left, top, right, bottom);
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposed) return;

        //    disposed = true;
        //    if (disposing)
        //    {
        //        if (backgroundDrawable != null)
        //        {
        //            backgroundDrawable.Dispose();
        //            backgroundDrawable = null;
        //        }
        //    }
        //    base.Dispose(disposing);
        //}
    }
}