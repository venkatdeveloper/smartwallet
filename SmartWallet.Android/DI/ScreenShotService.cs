﻿using System;
using System.IO;
using Android.App;
using Android.Graphics;
using SmartWallet.Droid.DI;
using SmartWallet.Services.ScreenShot;
using Xamarin.Forms;
using System.Threading.Tasks;

[assembly: Dependency(typeof(ScreenshotService))]
namespace SmartWallet.Droid.DI
{
    public class ScreenshotService : IScreenshotService
    {
        private  Activity _currentActivity;
        public void SetActivity(Activity activity) => _currentActivity = activity;

        public async Task<byte[]> Capture()
        {
            var activity = Forms.Context as MainActivity;
            if (activity == null)
            {
                return null;
            }
            var view = activity.Window.DecorView;
            view.DrawingCacheEnabled = true;
            Bitmap bitmap = view.GetDrawingCache(true);
            byte[] bitmapData;
            using (var stream = new MemoryStream())
            {
                bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                bitmapData = stream.ToArray();
                var backingFile = System.IO.Path.Combine(Xamarin.Essentials.FileSystem.AppDataDirectory, "count.png");
                using (var writer = File.CreateText(backingFile))
                {
                    await writer.WriteLineAsync(stream.ToString());
                }
            }

            return bitmapData;
        }
    }
}
