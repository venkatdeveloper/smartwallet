﻿using Android.Widget;
using SmartWallet.Droid.DI;
using SmartWallet.Services.Toast;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastHandler))]
namespace SmartWallet.Droid.DI
{
    public class ToastHandler : IToastService
    {
        public void ShowToast(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short).Show();
        }
    }
}