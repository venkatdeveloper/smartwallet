﻿using System;
using System.Collections;
using Xamarin.Forms;

namespace SmartWallet.CustomView
{
    public class HorizontalTabbedView : ScrollView
    {


        readonly StackLayout stackLayout;

        public static readonly BindableProperty ItemSourceProperty = BindableProperty.Create(
            nameof(ItemSource),
            typeof(IList),
            typeof(HorizontalTabbedView),
            null,
            propertyChanged: (bindable, oldValue, newValue) => ((HorizontalTabbedView)bindable).HandleItemSourcePropertyChanged(bindable, (IList)oldValue, (IList)newValue));

        public IList ItemSource
        {
            get => (IList)GetValue(ItemSourceProperty);
            set => SetValue(ItemSourceProperty, value);
        }

        public static readonly BindableProperty SelectedItemProperty = BindableProperty.Create(
            nameof(SelectedItem),
            typeof(object),
            typeof(HorizontalTabbedView),
            null);

        public object SelectedItem
        {
            get => GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public DataTemplate ItemTemplate { get; set; }

        public HorizontalTabbedView()
        {
            Orientation = ScrollOrientation.Horizontal;
            HorizontalScrollBarVisibility = ScrollBarVisibility.Never;

            stackLayout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Spacing = 5,
                Padding = new Thickness(8, 3)
            };
            Content = stackLayout;
        }

        void HandleItemSourcePropertyChanged(BindableObject bindable, IList oldValue, IList newValue)
        {
            stackLayout.Children.Clear();
            if (newValue == null && ItemTemplate == null)
                return;

            foreach (var item in newValue)
            {
                var view = (View)ItemTemplate.CreateContent();
                view.BindingContext = item;

                var tapGesture = new TapGestureRecognizer
                {
                    Command = new Command((obj) =>
                    {
                        SelectedItem = obj;
                    }),
                    CommandParameter = item
                };
                view.GestureRecognizers.Add(tapGesture);

                stackLayout.Children.Add(view);
                stackLayout.RaiseChild(view);
            }



        }

    }
}
