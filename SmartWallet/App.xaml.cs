﻿using System;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Services.Navigation;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartWallet
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            
            InitializeNavigation();
        }

        Task InitializeNavigation()
        {
            return Locator.Instance.Resolve<INavigationService>().InitializeAsync();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
