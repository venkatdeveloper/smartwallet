﻿using SmartWallet.Module.DTOs;

namespace SmartWallet.Extension
{
    public static class ResponseBaseExtension
    {
        public static bool IsSuccess(this ResponseBase responseBase)
        {
            return responseBase.StatusCode >= 200 && responseBase.StatusCode <= 299 && responseBase.Status.Equals("success", System.StringComparison.CurrentCultureIgnoreCase);
        }
    }
}