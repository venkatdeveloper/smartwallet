﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SmartWallet.Extension
{
    public static class UriBuilderExtensions
    {
        internal static void AddParameter(this UriBuilder builder, Dictionary<string, string> parameter)
        {
            if (parameter.Count == 0) return;

            var queryParameter = string.Empty;
            foreach (KeyValuePair<string, string> item in parameter)
            {
                if (string.IsNullOrEmpty(queryParameter) && string.IsNullOrEmpty(builder.Uri.Query))
                    queryParameter += $"?{item.Key}={item.Value}";
                else
                    queryParameter += $"&{item.Key}={item.Value}";
            }
            builder.Query = queryParameter;
        }

        internal static void AppendToPath(this UriBuilder builder, string pathToAdd) => builder.Path = Path.Combine(builder.Path, pathToAdd);
    }
}