﻿using System.Text.RegularExpressions;

namespace SmartWallet.Extension
{
    public static class StringExtension
    {
        public static bool IsValid(this string str)
        {
            return !(string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str));
        }

        public static bool IsValidMail(this string email)
        {
            if (!email.IsValid())
                return false;

            string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
            return Regex.IsMatch(email.Trim(), pattern);
        }
    }
}