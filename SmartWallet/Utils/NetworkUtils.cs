﻿using Xamarin.Essentials;

namespace SmartWallet.Utils
{
    public static class NetworkUtils
    {
        public static bool IsConnected => Connectivity.NetworkAccess == NetworkAccess.Internet;
    }
}