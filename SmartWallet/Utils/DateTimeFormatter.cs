﻿using System;
using System.Globalization;

namespace SmartWallet.Utils
{
    public static class DateTimeFormatter
    {
        public static string GetFormattedDate(string date, string format)
        {
            if (string.IsNullOrEmpty(date) ||
             string.IsNullOrWhiteSpace(date) ||
             string.IsNullOrEmpty(format) ||
             string.IsNullOrWhiteSpace(format))
                return string.Empty;

            DateTime currentDate = DateTime.Now;
            if (DateTime.TryParseExact(date, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out currentDate)) { }
            else if (DateTime.TryParseExact(date, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out currentDate)) { }
            else if (DateTime.TryParseExact(date, "MM/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out currentDate)) { }
            else if (DateTime.TryParseExact(date, "M/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out currentDate)) { }
            var formattedDate = currentDate.ToString(format, new CultureInfo("en"));
            return formattedDate;
        }
    }
}