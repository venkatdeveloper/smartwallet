﻿using System;
using Xamarin.Forms;

namespace SmartWallet.Renderer
{
    public class CorneredImage : Image
    {
        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(CornerRadius), typeof(CorneredImage), default(CornerRadius));
        public CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }
        public CorneredImage()
        {

        }
    }
}
