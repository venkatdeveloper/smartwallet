﻿using System;
using Xamarin.Forms;

namespace SmartWallet.Renderer
{
    public class GradientFrame: Frame
    {
        
        


            public static readonly BindableProperty StartColorProprty = BindableProperty.Create(nameof(StartColor), typeof(Color), typeof(GradientFrame), Color.Transparent);
        public Color StartColor
        {
            get => (Color)GetValue(StartColorProprty);
            set => SetValue(StartColorProprty, value);
        }

        public static readonly BindableProperty EndColorProprty = BindableProperty.Create(nameof(EndColor), typeof(Color), typeof(GradientFrame), Color.Transparent);
        public Color EndColor
        {
            get => (Color)GetValue(EndColorProprty);
            set => SetValue(EndColorProprty, value);
        }

        public static readonly BindableProperty IsBorderVisibleProprty = BindableProperty.Create(nameof(IsBorderVisible), typeof(bool), typeof(GradientFrame), false);
        public bool IsBorderVisible
        {
            get => (bool)GetValue(IsBorderVisibleProprty);
            set => SetValue(IsBorderVisibleProprty, value);
        }

        public static readonly BindableProperty IsVisibleGradientProperty = BindableProperty.Create(
            nameof(IsVisibleGradient),
            typeof(bool),
            typeof(GradientFrame),
            false);

        public bool IsVisibleGradient
        {
            get => (bool)GetValue(IsVisibleGradientProperty);
            set => SetValue(IsVisibleGradientProperty, value);
        }


        public static readonly BindableProperty BorderWidthProperty = BindableProperty.Create(
        nameof(BorderWidth),
        typeof(int),
        typeof(GradientFrame),
        1);

        public int BorderWidth
        {
            get => (int)GetValue(BorderWidthProperty);
            set => SetValue(BorderWidthProperty, value);
        }
    }
    
}
