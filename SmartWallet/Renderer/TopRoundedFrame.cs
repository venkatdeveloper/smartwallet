﻿using System;
using Xamarin.Forms;

namespace SmartWallet.Renderer
{

    public class TopRoundedFrame : Frame
    {
        public static new readonly BindableProperty CornerRadiusProperty = BindableProperty.Create(nameof(CornerRadius), typeof(CornerRadius), typeof(TopRoundedFrame), default(CornerRadius));
        public TopRoundedFrame()
        {
            base.CornerRadius = 0;
        }

        public new CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }
    }
   
}
