﻿using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Account
{
    public interface IAccountService
    {
        Task<DashboardResponse> DashboardDetailsAsync();
        Task<ResponseBase> ChangePasswordAsync(string oldPassword, string newpassword, string password_type);
        Task<ProfileResponse> GetProfileAsync();
        Task<ResponseBase> ChangePushNotificationAsync(string status);
        Task<AffiliateLinkResponse> GetAffiliateLinkAsync();
    }
}