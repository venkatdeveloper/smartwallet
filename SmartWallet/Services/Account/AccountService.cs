﻿using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Request;

namespace SmartWallet.Services.Account
{
    public class AccountService : IAccountService
    {
        readonly IRequestService requestService;
        public AccountService(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<ResponseBase> ChangePasswordAsync(string oldPassword, string newpassword, string password_type)
        {
            string api = Constants.CHANGEPASSWORD_API;
            api = password_type == "Modify Login" ? Constants.CHANGEPASSWORD_API : Constants.CHANGETRADEPASSWORD_API;
            var request = new RestRequest(api, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { oldpassword = oldPassword, newpassword });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        //public Task<ResponseBase> ChangeTradePasswordAsync(string oldPassword, string newPassword)
        //{
        //    var request = new RestRequest(Constants.CHANGETRADEPASSWORD_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { oldPassword, newPassword });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}

        public Task<DashboardResponse> DashboardDetailsAsync()
        {
            var request = new RestRequest(Constants.GET_DASHBOARD_DETAILS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<DashboardResponse>(request);
        }



        //public Task<ResponseBase> UpdateProfileAsync(UpdateProfileRequestModel profileModel)
        //{
        //    var request = new RestRequest(Constants.UPDATE_PROFILE_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(profileModel);
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}

        public Task<ProfileResponse> GetProfileAsync()
        {
            var request = new RestRequest(Constants.GET_PROFILE_DETAILS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ProfileResponse>(request);
        }





        //public Task<CountryListResponse> CountryLinkAsync()
        //{
        //    var request = new RestRequest(Constants.COUNTRY_LIST_API, HttpMethod.Get);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return requestService.ExecuteAsync<CountryListResponse>(request);
        //}



        public Task<ResponseBase> ChangePushNotificationAsync(string status)
        {
            var request = new RestRequest(Constants.CHANGE_PUSH_NOTIFICATION_STATUS_API, HttpMethod.Post);
            request.AddBody(new { status });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<AffiliateLinkResponse> GetAffiliateLinkAsync()
        {
            var request = new RestRequest(Constants.GET_AFFILIATE_LINK_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<AffiliateLinkResponse>(request);
        }
    }
}