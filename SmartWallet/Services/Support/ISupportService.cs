﻿using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Support
{
    public interface ISupportService
    {
        //Task<ChangeRequestHistoryResponse> GetChangeRequstHistoryAsync();
        //Task<ChangeRequestCategoryResponse> GetChangeRequstCategoriesAsync();
        //Task<ChangeRequestSingleItemResponse> GetChangeRequstHistoryByIdAsync(string id);
        Task<ResponseBase> CreateNewChangeRequestAsync(string filePath, string category, string details);
        Task<ResponseBase> SendReplyAsync(string details, string imageUrl, string tacId);
        Task<ResponseBase> UpdateKycAsync(string filePath);
        Task<KycResponse> GetKycAsync();
    }
}
