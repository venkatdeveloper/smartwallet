﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Request;
namespace SmartWallet.Services.Support
{
    public class SupportService : ISupportService
    {
        readonly IRequestService requestService;
        public SupportService(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public async Task<ResponseBase> CreateNewChangeRequestAsync(string filePath, string category, string details)
        {
            var request = new RestRequest(Constants.CREATE_NEW_CHANGE_REQUEST_API,HttpMethod.Post);//Constants.NEW_CHANGE_REQUEST_API
            request.AddParameter("Category",category);         
            request.AddParameter("Details",details);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            var files = new Dictionary<string, string> { { "image", filePath } };
            return await requestService.UploadAsync<ResponseBase>(request, files);
        }



        public async Task<ResponseBase> UpdateKycAsync(string filePath)
        {
            var request = new RestRequest(Constants.UPDATE_KYC_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            var files = new Dictionary<string, string> { { "IDcardFront", filePath },{"IDcardBack",filePath }, { "Selfi", filePath } };            
            return await requestService.UploadAsync<ResponseBase>(request, files);
        }








        //public async Task<ChangeRequestCategoryResponse> GetChangeRequstCategoriesAsync()
        //{
        //    var request = new RestRequest(Constants.CHANGE_REQUEST_CATEGORIES_API, HttpMethod.Get);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return await requestService.ExecuteAsync<ChangeRequestCategoryResponse>(request);
        //}

        //public async Task<ChangeRequestHistoryResponse> GetChangeRequstHistoryAsync()
        //{
        //    var request = new RestRequest(Constants.CHANGE_REQUEST_HISTORY_API, HttpMethod.Get);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return await requestService.ExecuteAsync<ChangeRequestHistoryResponse>(request);
        //}

        //public async Task<ChangeRequestSingleItemResponse> GetChangeRequstHistoryByIdAsync(string id)
        //{
        //    var request = new RestRequest(Constants.CHANGE_REQUEST_DETAILS_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { tacid = id });
        //    return await requestService.ExecuteAsync<ChangeRequestSingleItemResponse>(request);
        //}
        public async Task<ResponseBase> SendReplyAsync(string details,string imageUrl,string tacId)
        {
            var request = new RestRequest("", HttpMethod.Post);//Constants.CHANGE_REQUEST_REPLY_API
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddParameter("Details", details);
            request.AddParameter("token", AppSettings.Token);
            request.AddParameter("TicketID", tacId);
            var files = new Dictionary<string, string> { { "Attachment", imageUrl } };
            return await requestService.UploadAsync<ResponseBase>(request,files);
        }

        public async Task<KycResponse> GetKycAsync()
        {
            var request = new RestRequest(Constants.GET_KYC_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);            
            return await requestService.ExecuteAsync<KycResponse>(request);
        }
    }
}