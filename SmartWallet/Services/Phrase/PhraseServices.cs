﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Request;

namespace SmartWallet.Services.Phrase
{
    public class PhraseServices : IPhraseServices
    {
        readonly IRequestService requestService;
        public PhraseServices(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<ResponseBase> CreateMnemonicPhrase(string phrase)
        {
            var request = new RestRequest(Constants.CREATE_MNEMONIC_PHRASE_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { mnemonic = phrase });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<PhraseResponse> GetMneMonicPhraseAsync()
        {
            var request = new RestRequest(Constants.GET_MNEMONIC_PHRASE_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<PhraseResponse>(request);
        }

        public Task<PhraseResponse> ShowMneMonicPhraseAsync()
        {
            var request = new RestRequest(Constants.SHOW_MNEMONIC_PHRASE_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<PhraseResponse>(request);
        }

        public Task<ResponseBase> VerifyMneMonicPhraseAsync(string phrase)
        {
            var request = new RestRequest(Constants.VERIFY_MNEMONIC_PHRASE_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { mnemonic = phrase });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }
    }
}
