﻿using System;
using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Phrase
{
    public interface IPhraseServices
    {
        Task<PhraseResponse> GetMneMonicPhraseAsync();
        Task<PhraseResponse> ShowMneMonicPhraseAsync();
        Task<ResponseBase> VerifyMneMonicPhraseAsync(string phrase);
        Task<ResponseBase> CreateMnemonicPhrase(string phrase);
    }
}
