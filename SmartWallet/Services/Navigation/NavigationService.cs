﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using SmartWallet.Module.Common;
using SmartWallet.Module.Dashboard.View;
using SmartWallet.Module.Dashboard.ViewModel;
using SmartWallet.Module.ForgotPassword.View;
using SmartWallet.Module.ForgotPassword.ViewModal;
using SmartWallet.Module.Login.View;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Module.Profile.View;
using SmartWallet.Module.Profile.ViewModel;
using SmartWallet.Module.Register.View;
using SmartWallet.Module.Register.ViewModel;
using SmartWallet.Module.Root.View;
using SmartWallet.Module.Root.ViewModel;
using Xamarin.Forms;
using SmartWallet.Module.Assets.ViewModel;
using SmartWallet.Module.Assets.View;
using SmartWallet.Module.Transactions.ViewModel;
using SmartWallet.Module.Transactions.View;
using SmartWallet.Module.Market.ViewModel;
using SmartWallet.Module.Market.View;
using SmartWallet.Module.Startup.ViewModel;
using SmartWallet.Module.Startup.View;
using SmartWallet.Module.MasterStaking.ViewModel;
using SmartWallet.Module.MasterStaking.View;
using SmartWallet.Module.About.ViewModel;
using SmartWallet.Module.About.View;
using SmartWallet.Module.Affiliate.ViewModel;
using SmartWallet.Module.Affiliate.View;
using SmartWallet.Module.ChangePassword.ViewModel;
using SmartWallet.Module.ChangePassword.View;
using SmartWallet.Module.Withdraw.ViewModel;
using SmartWallet.Module.Withdraw.View;
using SmartWallet.Module.Community.ViewModel;
using SmartWallet.Module.Community.View;
using SmartWallet.Module.Deposit.ViewModel;
using SmartWallet.Module.Deposit.View;
using SmartWallet.Module.GenerationRevenue.ViewModel;
using SmartWallet.Module.GenerationRevenue.View;
using SmartWallet.Module.KYC.ViewModel;
using SmartWallet.Module.KYC.View;
using SmartWallet.Module.PersonalStaking.ViewModel;
using SmartWallet.Module.PersonalStaking.View;
using SmartWallet.Module.Revenue.ViewModel;
using SmartWallet.Module.Revenue.View;
using SmartWallet.Module.TotalRevenue.ViewModel;
using SmartWallet.Module.TotalRevenue.View;
using SmartWallet.Module.Swap.ViewModel;
using SmartWallet.Module.Swap.View;
using SmartWallet.Module.SplashScreen.ViewModel;
using SmartWallet.Module.SplashScreen.View;
using SmartWallet.Module.StakingRecord.ViewModel;
using SmartWallet.Module.StakingRecord.View;
using System.Linq;
using SmartWallet.Module.Phrase.ViewModel;
using SmartWallet.Module.Phrase.View;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Module.Country.View;
using SmartWallet.Module.Alert.ViewModel;
using SmartWallet.Module.Alert.View;

namespace SmartWallet.Services.Navigation
{
    public class NavigationService : INavigationService
    {

        protected readonly Dictionary<Type, Type> mapping;

        public ViewModelBase PreviousPageViewModel
        {
            get
            {
                //TODO reamining condition implement in feature
                Page page = null;
                if (Application.Current.MainPage is NavigationPage navigationPage)
                {
                    if (PopupNavigation.Instance.PopupStack.Count > 0)
                        page = navigationPage.Navigation.NavigationStack.Last();
                    else
                        page = navigationPage.Navigation.NavigationStack.ElementAtOrDefault(navigationPage.Navigation.NavigationStack.Count - 2);
                }
                return page?.BindingContext as ViewModelBase;
            }
        }

        //var mainPage = Application.Current.MainPage as CustomNavigationView;
        //var viewModel = mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2].BindingContext;
        //        return viewModel as ViewModelBase;




        public NavigationService()
        {
            mapping = new Dictionary<Type, Type>();
            CreatePageViewModelMapping();
        }

        public Task InitializeAsync()
        {
            //if (AppSettings.IsLogin)
            //{
            // if (Profiles.IsPinCreated)
            // return NavigateToAsync<PasscodeViewModel>(new PinNavigationData());
            // else
            // return NavigateToAsync<PasscodeViewModel>(new PinNavigationData { PinType = PinType.CREATE });
            //}
            //else
            //return NavigateToAsync<RootViewModel>();
            return NavigateToAsync<SplashScreenViewModel>();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase => InternalNavigateToAsync(typeof(TViewModel));

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase => InternalNavigateToAsync(typeof(TViewModel), parameter);

        public Task NavigateToAsync(Type viewModelType, bool isMenuType = false) => InternalNavigateToAsync(viewModelType, isMenuType: isMenuType);

        public Task NavigateToAsync(Type viewModelType, object parameter, bool isMenuType = false) => InternalNavigateToAsync(viewModelType, parameter, isMenuType);

        public Task NavigateModelAsync<TViewModel>() where TViewModel : ViewModelBase => InternalNavigateModelAsync(typeof(TViewModel));

        public Task NavigateModelAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase => InternalNavigateModelAsync(typeof(TViewModel), parameter);

        public Task NavigatePopupAsync<TViewModel>(bool animate = true) where TViewModel : ViewModelBase => NavigatePopupAsync<TViewModel>(null, animate);

        public Task NavigatePopupAsync<TViewModel>(object parameter, bool animate = true) where TViewModel : ViewModelBase
        {
            var page = CreateAndBindPage(typeof(TViewModel));
            if (page is PopupPage popupPage)
            {
                PopupNavigation.Instance.PushAsync(popupPage, animate);
            }
            return (page.BindingContext as ViewModelBase).InitializeAsync(parameter);
        }
        protected virtual Task InternalNavigateToAsync(Type viewModelType, object navigationData = null, bool isMenuType = false)
        {
            var page = CreateAndBindPage(viewModelType);
            if (page is SplashScreenPage)
            {
                var nav = new NavigationPage(page);
                nav.BarBackgroundColor = Color.FromHex("#0C2157");
                nav.BarTextColor = Color.White;
                Application.Current.MainPage = nav;
            }
            else if (page is LoginPage)
            {
                var nav = new NavigationPage(page);
                nav.BarBackgroundColor = Color.FromHex("#0C2157");
                nav.BarTextColor = Color.White;
                Application.Current.MainPage = nav;
            }
            //else if (page is PasscodePage)
            //{
            // if (Application.Current.MainPage is RootPage rootPage)
            // {
            // var dashboardPage = rootPage.Detail.Navigation.NavigationStack.FirstOrDefault(obj => obj is DashboardPage);
            // if (dashboardPage != null)
            // {
            // dashboardPage.Navigation.PushAsync(page);
            // }
            // else
            // {
            // Application.Current.MainPage = page;
            // }
            // }
            // else
            // {
            // Application.Current.MainPage = page;
            // }
            //}
            //else
            else if (page is RootPage)
            {
                var nav = new NavigationPage(page);
                nav.BarBackgroundColor = Color.FromHex("#0C2157");
                nav.BarTextColor = Color.White;
                Application.Current.MainPage = nav;
            }

            //else if (Application.Current.MainPage is RootPage rootPage)
            //{
            //    //rootPage.IsPresented = false;
            //    if (rootPage.Page is NavigationPage navigationPage && !isMenuType)
            //    {
            //        if (navigationPage.CurrentPage.GetType() != page.GetType())
            //        {
            //            navigationPage.PushAsync(page);
            //        }
            //    }
            //    else
            //    {
            //        rootPage.CurrentPage = new NavigationPage(page);
            //    }
            //}
            //else if (Application.Current.MainPage is LoginNavigationPage loginNavigationPage)
            //{
            // loginNavigationPage.PushAsync(page);
            //}
            else
            {
                Application.Current.MainPage.Navigation.PushAsync(page);
            }
            return (page.BindingContext as ViewModelBase).InitializeAsync(navigationData);
        }

        protected virtual Task InternalNavigateModelAsync(Type viewModelType, object navigationData = null)
        {
            var page = CreateAndBindPage(viewModelType);
            //if (Application.Current.MainPage is RootPage rootPage)
            //{
            // rootPage.IsPresented = false;
            // rootPage.Detail.Navigation.PushModalAsync(page);
            //}
            //else
            //{
            // Application.Current.MainPage.Navigation.PushModalAsync(page);
            //}
            return (page.BindingContext as ViewModelBase).InitializeAsync(navigationData);
        }

        public Task RemoveBackStackAsync()
        {
            throw new NotImplementedException();
        }

        public Task NavigateBackAsync() => InternalBackNavigation();

        public Task NavigateBackModelAsync() => InternalBackNavigation(true);

        protected virtual Task InternalBackNavigation(bool isModelPage = false)
        {
            if (Application.Current.MainPage is RootPage rootPage)
            {
                //if (rootPage.Detail is NavigationPage navigation)
                //{
                // if (isModelPage)
                // return navigation.Navigation.PopModalAsync();
                // else
                // return navigation.Navigation.PopAsync();
                //}
                //else
                //{
                // if (isModelPage)
                // return rootPage.Detail.Navigation.PopModalAsync();
                // else
                // return rootPage.Detail.Navigation.PopAsync();
                //}
                return rootPage.Navigation.PopAsync();
            }

            else
                return isModelPage
                ? Application.Current.MainPage?.Navigation.PopModalAsync()
                : Application.Current.MainPage?.Navigation.PopAsync();

        }

        protected Page CreateAndBindPage(Type viewModelType)
        {
            if (!mapping.ContainsKey(viewModelType))
            {
                throw new KeyNotFoundException($"No map for {viewModelType} was found on navigation mapping");
            }
            var pageType = mapping[viewModelType];

            var page = Activator.CreateInstance(pageType) as Page;
            var viewModel = Locator.Instance.Resolve(viewModelType);
            page.BindingContext = viewModel;
            return page;
        }

        void CreatePageViewModelMapping()
        {
            mapping.Add(typeof(LoginViewModel), typeof(LoginPage));
            mapping.Add(typeof(RootViewModel), typeof(RootPage));
            mapping.Add(typeof(AboutViewModel), typeof(AboutPage));
            mapping.Add(typeof(DashboardViewModel), typeof(DashboardPage));
            mapping.Add(typeof(AffiliateViewModel), typeof(AffiliatePage));
            mapping.Add(typeof(ChangePasswordViewModel), typeof(ChangePasswordPage));
            mapping.Add(typeof(CommunityViewModel), typeof(CommunityPage));
            mapping.Add(typeof(DepositViewModel), typeof(DepositPage));
            mapping.Add(typeof(ProfileViewModel), typeof(ProfilePage));
            mapping.Add(typeof(AssetViewModel), typeof(AssetPage));
            mapping.Add(typeof(TransactionViewModel), typeof(TransactionPage));
            mapping.Add(typeof(MarketViewModel), typeof(MarketPage));
            mapping.Add(typeof(StartupViewModel), typeof(StartupPage));
            mapping.Add(typeof(WithdrawViewModel), typeof(WithdrawPage));
            mapping.Add(typeof(SwapViewModel), typeof(SwapPage));
            //mapping.Add(typeof(TransferHistoryViewModel), typeof(TransferHistoryPage));
            mapping.Add(typeof(MasterStakingViewModel), typeof(MasterStakingPage));
            mapping.Add(typeof(GenerationRevenueViewModel), typeof(GenerationRevenuePage));
            //mapping.Add(typeof(AffiliateViewModel), typeof(AffiliatePage));
            mapping.Add(typeof(RegisterViewModal), typeof(RegisterPage));
            mapping.Add(typeof(ForgotPasswordViewModel), typeof(ForgotPasswordPage));
            mapping.Add(typeof(PersonalStakingViewModel), typeof(PersonalStakingPage));
            mapping.Add(typeof(StakingRecordViewModel), typeof(StakingRecordPage));
            mapping.Add(typeof(RevenueViewModel), typeof(RevenuePage));
            mapping.Add(typeof(TotalRevenueViewModel), typeof(TotalRevenuePage));
            //mapping.Add(typeof(ChangeRequestDetailViewModel), typeof(ChangeRequestDetailPage));
            mapping.Add(typeof(KYCViewModel), typeof(KYCPage));
            mapping.Add(typeof(SplashScreenViewModel), typeof(SplashScreenPage));
            mapping.Add(typeof(ForgetUserIdViewModel), typeof(ForgotUseridPage));
            mapping.Add(typeof(PhraseViewModel), typeof(PhrasePage));
            mapping.Add(typeof(CountryViewModel), typeof(CountryPage));
            mapping.Add(typeof(OtpPopupViewModel), typeof(OtpPopupPage));
        }
    }
}