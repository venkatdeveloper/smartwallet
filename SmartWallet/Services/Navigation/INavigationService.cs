﻿using System;
using System.Threading.Tasks;
using SmartWallet.Module.Common;

namespace SmartWallet.Services.Navigation
{
    public interface INavigationService
    {
        Task InitializeAsync();
        ViewModelBase PreviousPageViewModel { get; }
        Task NavigateToAsync<TViewModel>() where TViewModel : ViewModelBase;
        Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase;
        Task NavigateToAsync(Type viewModelType, bool isMenuType = false);
        Task NavigateToAsync(Type viewModelType, object parameter, bool isMenuType = false);
        Task NavigateModelAsync<TViewModel>() where TViewModel : ViewModelBase;
        Task NavigateModelAsync<TViewModel>(object parameter) where TViewModel : ViewModelBase;
        Task NavigatePopupAsync<TViewModel>(bool animate = true) where TViewModel : ViewModelBase;
        Task NavigatePopupAsync<TViewModel>(object parameter, bool animate = true) where TViewModel : ViewModelBase;
        Task NavigateBackModelAsync();
        Task NavigateBackAsync();
        Task RemoveBackStackAsync();
        
    }
}