﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using Newtonsoft.Json;
using Xamarin.Forms.Internals;

namespace SmartWallet.Services.Request
{
    public class RequestService : IRequestService
    {
        readonly JsonSerializerSettings serializerSettings;

        public RequestService()
        {
            serializerSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DateTimeZoneHandling = DateTimeZoneHandling.Local
            };
        }

        public async Task<TResult> ExecuteAsync<TResult>(IRestRequest request,string baseUrl = null) where TResult : ResponseBase
        {
            if (request.Method == HttpMethod.Post)
            {
                return await PostAsync<TResult>(request);
            }
            return await GetAsync<TResult>(request);
        }

        public async Task<TResult> UploadAsync<TResult>(IRestRequest request, Dictionary<string, string> files, string baseUrl = null) where TResult : ResponseBase
            => await UploadFile<TResult>(request, files, baseUrl);
        //Uploadfile ONLY FOR Postasync



        async Task<TResult> GetAsync<TResult>(IRestRequest request) where TResult : ResponseBase
        {
            var client = CreateClient();
            var uri = BuildUri(Constants.BASE_URL, request.Path, request.Parameter);
            request.Header.ForEach((obj) => client.DefaultRequestHeaders.Add(obj.Key, obj.Value));
            var httpResponseMessage = await client.GetAsync(uri);
            return await HandleResponseAsync<TResult>(httpResponseMessage);
        }

        async Task<TResult> PostAsync<TResult>(IRestRequest request) where TResult : ResponseBase
        {
            var client = CreateClient();
            request.Header.ForEach((obj) => client.DefaultRequestHeaders.Add(obj.Key, obj.Value));
            var uri = BuildUri(Constants.BASE_URL, request.Path, request.Parameter);
            var serialized = await Task.Run(() => JsonConvert.SerializeObject(request.Body, serializerSettings));
            var bodyContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            var httpResponseMessage = await client.PostAsync(uri, bodyContent);
            return await HandleResponseAsync<TResult>(httpResponseMessage);
        }

        async Task<TResult> UploadFile<TResult>(IRestRequest request, Dictionary<string, string> files, string baseUrl = null) where TResult : ResponseBase
        {
            var client = CreateClient();
            var requestContent = new MultipartFormDataContent();
            foreach (var file in files)
            {
                if (file.Value != string.Empty)
                {
                    var fileByteArray = await Task.Run(() => File.ReadAllBytes(file.Value));
                    var fileContent = new ByteArrayContent(fileByteArray);
                    fileContent.Headers.Add("Content-Disposition", $"form-data; name=\"{file.Key}\"; filename=\"{Path.GetFileName(file.Value)}\"");
                    fileContent.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
                    requestContent.Add(fileContent, file.Key, Path.GetFileName(file.Value));
                }
            }

            request.Parameter.ForEach((obj) =>
            {
                var stringContent = new StringContent(obj.Value);
                stringContent.Headers.Add("Content-Disposition", $"form-data; name=\"{obj.Key}\"");
                requestContent.Add(stringContent, obj.Key);
            });
            request.Header.ForEach((obj) => client.DefaultRequestHeaders.Add(obj.Key, obj.Value));
            var uri = BuildUri(baseUrl, request.Path, request.Parameter);
            var httpResponseMessage = await client.PostAsync(uri, requestContent);

            return await HandleResponseAsync<TResult>(httpResponseMessage);
        }

        async Task<TResult> HandleResponseAsync<TResult>(HttpResponseMessage httpResponseMessage) where TResult : ResponseBase
        {
            if (!httpResponseMessage.IsSuccessStatusCode)
                return (TResult)new ResponseBase { StatusCode = (int)httpResponseMessage.StatusCode, Message = httpResponseMessage.StatusCode.ToString() };
            var responseString = await httpResponseMessage.Content.ReadAsStringAsync();
            var result = await Task.Run(() => JsonConvert.DeserializeObject<TResult>(responseString, serializerSettings));
            result.StatusCode = (int)httpResponseMessage.StatusCode;
            return result;
        }

        Uri BuildUri(string baseUrl, string path, Dictionary<string, string> parameter)
        {
            UriBuilder uriBuilder = new UriBuilder(Constants.BASE_URL);
            uriBuilder.AppendToPath(path);
            uriBuilder.AddParameter(parameter);
            return uriBuilder.Uri;
        }

        HttpClient CreateClient()
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return httpClient;
        }
    }
}