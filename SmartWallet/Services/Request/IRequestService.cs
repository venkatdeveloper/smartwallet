﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Request
{
    public interface IRequestService
    {
        Task<TResult> ExecuteAsync<TResult>(IRestRequest request, string baseUrl = null) where TResult : ResponseBase;
        Task<TResult> UploadAsync<TResult>(IRestRequest request, Dictionary<string, string> files, string baseUrl = null) where TResult : ResponseBase;
    }
}