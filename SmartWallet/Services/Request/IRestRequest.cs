﻿using System.Collections.Generic;
using System.Net.Http;

namespace SmartWallet.Services.Request
{
    public interface IRestRequest
    {
        string Path { get; }
        Dictionary<string, string> Parameter { get; }
        Dictionary<string, string> Header { get; }
        object Body { get; }
        HttpMethod Method { get; }

        void AddBody(object value);
        void AddParameter(string key, string value);
        void AddHeader(string key, string value);
    }
}