﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace SmartWallet.Services.Dialog
{
    public class DialogService : IDialogService
    {
        public Task ShowAlertAsync(string title, string message, string cancel)
            => Application.Current.MainPage.DisplayAlert(title, message, cancel);

        public Task<bool> ShowConfimationAsync(string title, string message, string accept, string cancel)
            => Application.Current.MainPage.DisplayAlert(title, message, accept, cancel);

        public Task<string> ShowActionSheetAsync(string title, string cancel, string destruction, params string[] buttons)
            => Application.Current.MainPage.DisplayActionSheet(title, cancel, destruction, buttons);
    }
}