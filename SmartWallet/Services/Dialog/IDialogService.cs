﻿using System.Threading.Tasks;

namespace SmartWallet.Services.Dialog
{
    public interface IDialogService
    {
        Task ShowAlertAsync(string title, string message, string cancel);
        Task<bool> ShowConfimationAsync(string title, string message, string accept, string cencel);
        Task<string> ShowActionSheetAsync(string title, string cancel, string destruction, params string[] buttons);
    }
}