﻿using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Transaction
{
    public interface ITransactionService
    {
        Task<CoinRateResponse> GetCoinRateAsync(string coincode);
        Task<TransactionHistoryResponse> GetTransactionHistoryAsync(string coincode);
        Task<DepositResponse> DepositAsync(string coincode);

        //Task<SwapResponse> SwapAsync(string my_wallet_address, string amount, string from_coin, string to_coin);

        //Task<TransactionHistoryResponse> GetTransferHistoryAsync(string address, string coin_type);
        //Task<WithdrawResponse> WithdrawVerificationAsync(string my_wallet_address, string wallet_address, string amount, string coinType);


        //Task<Deposit> DepositAsync(string usdAmount);
        Task<ResponseBase> WithdrawVerificationAsync(string amount, string walletaddress, string coincode);
        Task<ResponseBase> WithdrawConfirmationAsync(string amount, string walletaddress, string coincode, string otp);
        Task<ResponseBase> SwapVerificationAsync(string amount);
        Task<ResponseBase> SwapConfirmationAsync(string amount);
        Task<ResponseBase> WithdrawReportAsync();
        Task<ResponseBase> SwapReportAsync();
        //Task<Withdraw> WithdrawConfirmationAsync(string amount, string address, string withdrawType, string otp, string tag);
        //Task<TransferHistoryResponse> GetTransferHistoryAsync(Currency currencyType);
        //Task<Transfer> TransferVerificationAsync(Currency currencyType, string amount, string transferTo, string fee = null);
        //Task<Transfer> TransferConfirmationAsync(Currency currencyType, string amount, string transferTo, string fee = null, string otp = null);
    }
}