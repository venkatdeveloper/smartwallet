﻿using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Request;

namespace SmartWallet.Services.Transaction
{
    public class TransactionService : ITransactionService
    {
        readonly IRequestService requestService;

        public TransactionService(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        public Task<DepositResponse> DepositAsync(string coincode)
        {
            var request = new RestRequest(Constants.DEPOSIT_API, HttpMethod.Post);
            request.AddBody(new { coincode });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<DepositResponse>(request);
        }

        public Task<CoinRateResponse> GetCoinRateAsync(string coincode)
        {
            var request = new RestRequest(Constants.GET_INDIVIDUAL_COIN_DETAILS_API, HttpMethod.Post);
            request.AddBody(new { coincode });
            return requestService.ExecuteAsync<CoinRateResponse>(request);
        }

        public Task<TransactionHistoryResponse> GetTransactionHistoryAsync(string coincode)
        {
            var request = new RestRequest(Constants.GET_TRANSACTIONS_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { coincode });
            return requestService.ExecuteAsync<TransactionHistoryResponse>(request);
        }

        public Task<ResponseBase> SwapVerificationAsync(string amount)
        {
            var request = new RestRequest(Constants.SWAP_REQUEST_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { amount });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> SwapReportAsync()
        {
            var request = new RestRequest(Constants.SWAP_REPORT_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);            
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> SwapConfirmationAsync(string amount)
        {
            var request = new RestRequest(Constants.SWAP_CONFIRM_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { amount });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> WithdrawConfirmationAsync(string amount, string walletaddress, string coincode, string otp)
        {
            var request = new RestRequest(Constants.WITHDRAWAL_REQUEST_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { amount, walletaddress, coincode, OTP = otp });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> WithdrawReportAsync()
        {
            var request = new RestRequest(Constants.WITHDRAWAL_REPORT_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> WithdrawVerificationAsync(string amount, string walletaddress, string coincode)
        {
            var request = new RestRequest(Constants.WITHDRAWAL_CONFIRM_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { amount, walletaddress, coincode });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        //public Task<Deposit> DepositAsync(string usdAmount)
        //{
        //    var request = new RestRequest(Constants.DEPOSIT_API, HttpMethod.Post);
        //    request.AddBody(new { usd = usdAmount });
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return requestService.ExecuteAsync<Deposit>(request);



        //public Task<SwapResponse> SwapAsync(string my_wallet_address, string amount, string from_coin, string to_coin)
        //{
        //    var request = new RestRequest(Constants.SWAP_API, HttpMethod.Post);
        //    request.AddBody(new { my_wallet_address, amount, from_coin, to_coin });
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return requestService.ExecuteAsync<SwapResponse>(request);
        //}







        //public Task<WithdrawResponse> WithdrawVerificationAsync(string my_wallet_address, string wallet_address, string amount,string coinType)
        //{
        //   var request = new RestRequest(Constants.WITHDRAW_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //   request.AddBody(new { my_wallet_address, wallet_address, amount, coinType});
        //    return requestService.ExecuteAsync<WithdrawResponse>(request);
        //}







        //public Task<Withdraw> WithdrawVerificationAsync(string amount, string address, string withdrawType)
        //{
        //    var request = new RestRequest(Constants.USD_WITHDRAW_VERIFICATION_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { amount, walletaddress = address, method = withdrawType });
        //    return requestService.ExecuteAsync<Withdraw>(request);
        //}

        //public Task<Withdraw> WithdrawConfirmationAsync(string amount, string address, string withdrawType, string otp, string tag)
        //{
        //    var request = new RestRequest(Constants.USD_WITHDRAW_CONFIRMATION_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { amount, walletaddress = address, method = withdrawType, OTP = otp, newtag = tag });
        //    return requestService.ExecuteAsync<Withdraw>(request);
        //}



        //public Task<TransactionHistoryResponse> GetTransferHistoryAsync(string address, string coin_type)
        //{

        //    var request = new RestRequest(Constants.TRANSACTION_HISTORY_API, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { address, coin_type});
        //    return requestService.ExecuteAsync<TransactionHistoryResponse>(request);
        //}




        //public Task<Transfer> TransferVerificationAsync(Currency currencyType, string amount, string transferTo, string fee = null)
        //{
        //    var endPoint = currencyType == Currency.XRP ? Constants.TRANSFER_XRP_VERIFICATION_API : Constants.TRANSFER_USD_VERIFICATION_API;
        //    var request = new RestRequest(endPoint, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { transferamt = amount, transferto = transferTo, TransactionFee = fee });
        //    return requestService.ExecuteAsync<Transfer>(request);
        //}

        //public Task<Transfer> TransferConfirmationAsync(Currency currencyType, string amount, string transferTo, string fee = null, string otp = null)
        //{
        //    var endPoint = currencyType == Currency.XRP ? Constants.TRANSFER_XRP_CONFIRMATION_API : Constants.TRANSFER_USD_CONFIRMATION_API;
        //    var request = new RestRequest(endPoint, HttpMethod.Post);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    request.AddBody(new { transferamt = amount, transferto = transferTo, TransactionFee = fee, tac = otp });
        //    return requestService.ExecuteAsync<Transfer>(request);
        //}
    }
}