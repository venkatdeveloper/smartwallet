﻿using System;
using System.Threading.Tasks;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Services.Market
{
    public interface IMarketService
    {

        Task<StakingDetailsResponse> StakingDetailsAsync();
        Task<MasterStakingResponse> MasterStakingAsync(string Amount, string coin_type);
        Task<ResponseBase> MasterStakingConfirmationAsync(string Amount, string coin_type);
        Task<StakingHistoryResponse> StakingHistoryAsync();
        Task<CommunityListResponse> GetCommunityAsync();
        Task<RevenueDetailsResponse> GetRevenueAsync();
        Task<PersonalStakingResponse> GetPersonalStakingAsync();
        //Task<MasterNodesResponse> MasterNodesAsync();
        //Task<RevenueDetailsResponse> RevenueDetailsAsync();
        //Task<RevenueHistoryResponse> RevenueHistoryAsync(string revenue_type);
        //Task<RequestCoinResponse> RequestCoinAsync(string name, string mobile, string address);
    }
}
