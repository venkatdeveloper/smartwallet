﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Account;
using SmartWallet.Services.Request;

namespace SmartWallet.Services.Market
{
    public class MarketService : IMarketService
    {
        readonly IRequestService requestService;
        public MarketService(IRequestService requestService)
        {
            this.requestService = requestService;
        }


        public Task<StakingDetailsResponse> StakingDetailsAsync()
        {
            var request = new RestRequest(Constants.GET_NG_DETAILS_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<StakingDetailsResponse>(request);
        }


        public Task<MasterStakingResponse> MasterStakingAsync(string Amount, string coin_type)
        {
            var request = new RestRequest(Constants.BUY_MASTER_STACKING_API, HttpMethod.Post);
            request.AddBody(new { coincode = coin_type, amount = Amount });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<MasterStakingResponse>(request);
        }
        public Task<ResponseBase> MasterStakingConfirmationAsync(string Amount, string coin_type)
        {
            var request = new RestRequest(Constants.BUY_MASTER_STACKING_CONFIRM_API, HttpMethod.Post);
            request.AddBody(new { coincode = coin_type, amount = Amount });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        public Task<StakingHistoryResponse> StakingHistoryAsync()
        {
            var request = new RestRequest(Constants.BUY_HISTORY_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<StakingHistoryResponse>(request);
        }

        public Task<MasterNodesResponse> MasterNodesAsync()
        {
            var request = new RestRequest(Constants.BUY_HISTORY_API, HttpMethod.Get);

            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<MasterNodesResponse>(request);
        }

        //public Task<RevenueDetailsResponse> RevenueDetailsAsync()
        //{
        //    var request = new RestRequest(Constants.BUY_HISTORY_API, HttpMethod.Get);
        //    request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
        //    return requestService.ExecuteAsync<RevenueDetailsResponse>(request);
        //}

        public Task<RevenueHistoryResponse> RevenueHistoryAsync(string revenue_type)
        {
            var request = new RestRequest(Constants.BUY_HISTORY_API, HttpMethod.Get);
            request.AddBody(new { revenue_type });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<RevenueHistoryResponse>(request);
        }

        public Task<RequestCoinResponse> RequestCoinAsync(string name, string mobile, string address)
        {
            var request = new RestRequest(Constants.BUY_HISTORY_API, HttpMethod.Post);
            request.AddBody(new { name, mobile, address });
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<RequestCoinResponse>(request);
        }

        public Task<CommunityListResponse> GetCommunityAsync()
        {
            var request = new RestRequest(Constants.GET_COMMUNITY_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<CommunityListResponse>(request);
        }

        public Task<RevenueDetailsResponse> GetRevenueAsync()
        {
            var request = new RestRequest(Constants.GET_REVENUE_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<RevenueDetailsResponse>(request);
        }

        public Task<PersonalStakingResponse> GetPersonalStakingAsync()
        {
            var request = new RestRequest(Constants.GET_PERSONALREVENUE_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<PersonalStakingResponse>(request);
        }
    }
}
