﻿namespace SmartWallet.Services.Toast
{
    public interface IToastService
    {
        void ShowToast(string message);
    }
}