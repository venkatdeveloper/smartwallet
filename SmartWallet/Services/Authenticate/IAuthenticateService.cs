﻿using System.Threading.Tasks;
using SmartWallet.Module.DTOs;
using SmartWallet.Module.Register.Model;

namespace SmartWallet.Services.Authenticate
{
    public interface IAuthenticateService
    {        
        Task<ResponseBase> VerifyUseridAsync(string userid);
        Task<LoginResponse> MemberLoginAsync(string username, string password);
        Task<ResponseBase> RegisterAsync(RegisterRequestModel requestModel,int step);
        Task<ResponseBase> EmailVerificationAsync();
        Task<ResponseBase> SubmitEmailVerificationAsync(string EmailCode);
        Task<ResponseBase> ForgotUserIdByEmailAsync(string Email);
        Task<ResponseBase> ForgotUserIdByMneMonicAsync(string Phrase);
        Task<ResponseBase> ForgotPasswordMneMonicAsync(string phrase, string username);
        Task<ResponseBase> ForgotPasswordEmailAsync(string Email,string username);
        Task<CountryListResponse> GetCountryListAsync();        
    }
}