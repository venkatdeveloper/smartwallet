﻿using System.Net.Http;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Module.Register.Model;
using SmartWallet.Services.Request;

namespace SmartWallet.Services.Authenticate
{
    public class AuthenticateService : IAuthenticateService
    {
        readonly IRequestService requestService;

        public AuthenticateService(IRequestService requestService)
        {
            this.requestService = requestService;
        }

        //public Task<ResponseBase> CheckVerificationCode(string verify_code, string emailid)
        //{
        //    var request = new RestRequest(Constants.CHECK_VERIFY_CODE, HttpMethod.Post);
        //    request.AddBody(new { verifycode = verify_code, emailid = emailid });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}

        //public Task<ResponseBase> GetMneMonicPhrase(string userid)
        //{
        //    var request = new RestRequest(Constants.GET_MNEMONIC_PHRASE_API, HttpMethod.Post);
        //    request.AddBody(new { userid = userid });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}
        public Task<ResponseBase> VerifyUseridAsync(string userid)
        {
            var request = new RestRequest(Constants.VERIFY_USER_ID_API, HttpMethod.Post);
            request.AddBody(new { username = userid });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        public Task<LoginResponse> MemberLoginAsync(string username, string password)
        {
            var request = new RestRequest(Constants.LOGIN_API, HttpMethod.Post);
            request.AddBody(new {username, password });
            return requestService.ExecuteAsync<LoginResponse>(request);
        }

        public Task<ResponseBase> RegisterAsync(RegisterRequestModel requestModel, int step)
        {
            string api = Constants.REGISTRATION_FIRST_STEP_API;
            switch (step)
            {
                case 1:
                    {
                        api = Constants.SIGNUP_SMS_API;
                        break;
                    }
                case 2:
                    {
                        api = Constants.REGISTRATION_FIRST_STEP_API;
                        break;
                    }
                case 3:
                    {
                        api = Constants.REGISTRATION_PERSONAL_INFO_API;
                        break;
                    }
                case 4:
                    {
                        api = Constants.REGISTRATION_LOGIN_PWD_API;
                        break;
                    }
                case 5:
                    {
                        api = Constants.REGISTRATION_TRADING_PWD_API;
                        break;
                    }
            }
            var request = new RestRequest(api, HttpMethod.Post);
            request.AddBody(requestModel);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> EmailVerificationAsync()
        {
            var request = new RestRequest(Constants.EMAIL_VERIFICATION_API, HttpMethod.Get);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> SubmitEmailVerificationAsync(string EmailCode)
        {
            var request = new RestRequest(Constants.EMAIL_VERIFICATION_SUBMIT_API, HttpMethod.Post);
            request.AddHeader(Constants.AUTHORIZATION_KEY, AppSettings.Token);
            request.AddBody(new { emailcode = EmailCode });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> ForgotUserIdByEmailAsync(string Email)
        {
            var request = new RestRequest(Constants.FORGOT_USER_ID_BY_EMAIL_API, HttpMethod.Post);
            request.AddBody(new { email = Email });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }
        public Task<ResponseBase> ForgotUserIdByMneMonicAsync(string Phrase)
        {
            var request = new RestRequest(Constants.FORGOT_USER_ID_BY_MNEMONIC_API, HttpMethod.Post);
            request.AddBody(new { mnemonic = Phrase });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }
        public Task<CountryListResponse> GetCountryListAsync()
        {
            var request = new RestRequest(Constants.GET_COUNTRY_LIST_API, HttpMethod.Get);            
            return requestService.ExecuteAsync<CountryListResponse>(request);
        }

        public Task<ResponseBase> ForgotPasswordEmailAsync(string Email, string username)
        {
            var request = new RestRequest(Constants.FORGOT_PASSWORD_BY_EMAIL_API, HttpMethod.Post);
            request.AddBody(new { email = Email, username });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }

        public Task<ResponseBase> ForgotPasswordMneMonicAsync(string phrase, string username)
        {
            var request = new RestRequest(Constants.FORGOT_ForgotPassword_BY_MNEMONIC_API, HttpMethod.Post);
            request.AddBody(new { mnemonic = phrase, username });
            return requestService.ExecuteAsync<ResponseBase>(request);
        }


        //public Task<LoginResponse> MemberLoginAsync(string userid, bool isFingerprintSuccess)
        //{
        //    var request = new RestRequest(Constants.LOGIN_API, HttpMethod.Post);
        //    request.AddBody(new { userid ,isfingerprintsuccess = isFingerprintSuccess });
        //    return requestService.ExecuteAsync<LoginResponse>(request);
        //}

        //public Task<ResponseBase> MemberRegisterationAsync(string referralid, string email_id, string loginPassword, string paymentpassword)
        //{
        //    var request = new RestRequest(Constants.REGISTER_API, HttpMethod.Post);
        //    request.AddBody(new { referralid, emailid = email_id,loginpassword = loginPassword,paymentpassword});
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}

        //public Task<ResponseBase> RecoverPassword(string emailid)
        //{
        //    var request = new RestRequest(Constants.FORGOTPASSWORD_API, HttpMethod.Post);
        //    request.AddBody(new { email_id =  emailid });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}

        //public Task<ResponseBase> RecoverUserId(string emailid)
        //{
        //    var request = new RestRequest(Constants.FORGOT_USERID_API, HttpMethod.Post);
        //    request.AddBody(new { email_id = emailid });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}



        //public Task<ResponseBase> VerifyReferralCode(string referralid)
        //{
        //    var request = new RestRequest(Constants.VERIFY_REFERRAL_CODE, HttpMethod.Post);
        //    request.AddBody(new { referralid });
        //    return requestService.ExecuteAsync<ResponseBase>(request);
        //}






    }
}