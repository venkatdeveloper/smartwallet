﻿using System;
using System.Threading.Tasks;

namespace SmartWallet.Services.ScreenShot
{
    public interface IScreenshotService
    {
        Task<byte[]> Capture();
    }
}
