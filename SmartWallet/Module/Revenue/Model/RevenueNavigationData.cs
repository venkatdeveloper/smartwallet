﻿using System;
namespace SmartWallet.Module.Revenue.Model
{
    public class RevenueNavigationData

    {
        public string Amount { get; set; }
        public string Name { get; set; }

        public RevenueNavigationData()
        {
        }
    }
}
