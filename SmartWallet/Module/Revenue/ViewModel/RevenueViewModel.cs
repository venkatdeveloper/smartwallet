﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Module.Common;
using SmartWallet.Module.PersonalStaking.ViewModel;
using SmartWallet.Utils;
using SmartWallet.Module.GenerationRevenue.ViewModel;
using SmartWallet.Module.TotalRevenue.ViewModel;
using SmartWallet.Module.Revenue;
using SmartWallet.Module.Revenue.Model;
using SmartWallet.Services.Market;
using SmartWallet.Extension;

namespace SmartWallet.Module.Revenue.ViewModel
{
    public class RevenueViewModel:ViewModelBase
    {
        readonly IMarketService marketService;

        string totalrevenue="0.00";
        public string Totalrevenue
        {
            get => totalrevenue;
            set
            {
                totalrevenue = value;
                OnPropertyChanged();
            }

        }


        string personalrevenue;
        public string Personalrevenue
        {
            get => personalrevenue;
            set
            {
                personalrevenue = value;
                OnPropertyChanged();
            }

        }


        string generationrevenue;
        public string Generationrevenue
        {
            get => generationrevenue;
            set
            {
                generationrevenue = value;
                OnPropertyChanged();
            }

        }


        string communityrevenue;
        public string Communityrevenue
        {
            get => communityrevenue;
            set
            {
                communityrevenue = value;
                OnPropertyChanged();
            }

        }

        string loyaltyrevenue;
        public string Loyaltyrevenue
        {
            get => loyaltyrevenue;
            set
            {
                loyaltyrevenue = value;
                OnPropertyChanged();
            }

        }


        string totalintersellarrevenue;
        public string Totalintersellarrevenue
        {
            get => totalintersellarrevenue;
            set
            {
                totalintersellarrevenue = value;
                OnPropertyChanged();
            }

        }


        string stakingrevenuerate;
        public string Stakingrevenuerate
        {
            get => stakingrevenuerate;
            set
            {
                stakingrevenuerate = value;
                OnPropertyChanged();
            }

        }


        public RevenueViewModel(IMarketService marketService)
        {
            this.marketService = marketService;
        }

        public override Task InitializeAsync(object navigationData) => InitialSetup();

        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.GetRevenueAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
               // ToastService.ShowToast(response.Message);
                Personalrevenue = response.TotalPersonalRevenue;
                Communityrevenue = response.TotalCommunityRevenue;
                Totalrevenue = response.AvailableRevenue;
                return;
            }

            Personalrevenue = response.TotalPersonalRevenue;
            //Generationrevenue = response.GenerationRevenue;
            Communityrevenue = response.TotalCommunityRevenue;
            //Loyaltyrevenue = response.LoyaltyRevenue;
            //Totalintersellarrevenue = response.TotalInterstellarRevenue;
            //Stakingrevenuerate = response.TotalStakingRate;
            Totalrevenue = response.AvailableRevenue;
        }






        public ICommand GotoPersonalstakingcmd => new AsyncCommand(OnPersonalstaking);
        public ICommand GotoGeneralrevenuecmd => new AsyncCommand(OnGeneralrevenue);
        public ICommand GotoCommunityrevenuecmd => new AsyncCommand(OnCommunityrevenue);
        public ICommand GotoLoyaltyrevenuecmd => new AsyncCommand(OnLoyaltyrevenue);
        public ICommand GotoTotalrevenuecmd => new AsyncCommand(OnTotalrevenue);









        async Task OnPersonalstaking()=> await NavigationService.NavigateToAsync<PersonalStakingViewModel>(Personalrevenue);


        

        async Task OnGeneralrevenue()
        {
            var data = new RevenueNavigationData
            {
                Amount = Generationrevenue,
                Name = "GENERATION"
            };
            await NavigationService.NavigateToAsync<GenerationRevenueViewModel>(data);
        }


       

        async Task OnCommunityrevenue()
        {
            var data = new RevenueNavigationData
            {
                Amount = Communityrevenue,
                Name = "COMMUNITY"
            };
            await NavigationService.NavigateToAsync<GenerationRevenueViewModel>(data);
        }


       

        async Task OnLoyaltyrevenue()
        {
            var data = new RevenueNavigationData
            {
                Amount = Loyaltyrevenue,
                Name = "LOYALTY"
            };
            await NavigationService.NavigateToAsync<GenerationRevenueViewModel>(data);
        }



        

        async Task OnTotalrevenue() => await NavigationService.NavigateToAsync<TotalRevenueViewModel>();
    }


}


