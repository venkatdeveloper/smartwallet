﻿using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Module.Common;
using SmartWallet.Utils;

namespace SmartWallet.Module.Startup.ViewModel
{
    public class StartupViewModel : ViewModelBase
    {
        public ICommand SignInCmd => new AsyncCommand(SignIn);
        public ICommand SignUpCmd => new AsyncCommand(SignUp);
        public StartupViewModel()
        {
        }

        async Task SignIn()
        {
            //await NavigationService.NavigateToAsync<LoginViewModel>();
            await Task.CompletedTask;
        }
        async Task SignUp()
        {
            // await NavigationService.NavigateToAsync<RegisterViewModel>();
            await Task.CompletedTask;
        }
    }
}
