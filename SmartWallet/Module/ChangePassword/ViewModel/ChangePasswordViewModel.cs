﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Services.Account;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;

namespace SmartWallet.Module.ChangePassword.ViewModel
{
    public class ChangePasswordViewModel : ViewModelBase
      
    {
        readonly IAccountService accountService;
        readonly IAuthenticateService authenticateService;

        string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }


        //bool modifypassword;
        //public bool Modifypassword
        //{
        //    get => modifypassword;
        //    set
        //    {
        //        modifypassword = value;
        //        OnPropertyChanged();

        //    }
        //}

        string oldPassword;
        public string OldPassword
        {
            get => oldPassword;
            set
            {
                oldPassword = value;
                OnPropertyChanged();

            }
        }

        string password;
        public string Password
        {
            get => password;
            set
            {
                password = value;
                OnPropertyChanged();

            }
        }

        string confirmPassword;
        public string ConfirmPassword
        {
            get => confirmPassword;
            set
            {
                confirmPassword = value;
                OnPropertyChanged();

            }
        }

        //string verificationCode;
        //public string VerificationCode
        //{
        //    get => verificationCode;
        //    set
        //    {
        //        verificationCode = value;
        //        OnPropertyChanged();

        //    }
        //}

       // public ICommand NextCmd => new AsyncCommand(OnNext);
        public ICommand FinishCmd => new AsyncCommand(OnFinish);
       // public ICommand RequestCodeCmd => new AsyncCommand(OnRequestCode);

        public ChangePasswordViewModel(IAccountService accountService,IAuthenticateService authenticateService)
        {
            this.accountService = accountService;
            this.authenticateService = authenticateService;
           // Modifypassword = true;
        }


        //async Task OnRequestCode()
        //{
        //    if (!NetworkUtils.IsConnected)
        //    {
        //        ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
        //        return;
        //    }
        //    IsBusy = true;
        //    //var response = await authenticateService.GetVerifyCodeAsync(AppSettings.UserName);
        //    IsBusy = false;
        //    //ToastService.ShowToast(response.Message);

        //}




        //async Task OnNext()
        //{
        //    if (!IsValidVerifyData()) return;
        //    Modifypassword = false;
        //    Resetpassword = true;

        //    await Task.CompletedTask;

        //}




        //bool IsValidVerifyData()
        //{
        //    var isValid = false;
        //    if (!VerificationCode.IsValid())
        //        ToastService.ShowToast(Constants.EMPTY_VERIFICATIONCODE);

        //    else isValid = true;
        //    return isValid;
        //}




        async Task OnFinish()
        {
            if (!IsValidPasswordData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await accountService.ChangePasswordAsync(OldPassword,Password,Name);
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
            await NavigationService.NavigateBackAsync();
        }

        bool IsValidPasswordData()
        {
            var isValid = false;
            if (!Password.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (!ConfirmPassword.IsValid())
                ToastService.ShowToast(Constants.EMPTY_CONFIRMPASSWORD);
            else if (ConfirmPassword != Password)
                ToastService.ShowToast(Constants.PASSWORD_MISMATCH);
            else isValid = true;
            return isValid;
        }


        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);
        async Task InitialSetup(object data)
        {
            if (data != null)
            {
                Name = data.ToString();
            }
            await Task.CompletedTask;
        }

    }

}   

