﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Alert.ViewModel;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Services.Transaction;
using SmartWallet.Utils;

namespace SmartWallet.Module.Swap.ViewModel
{
    public class SwapViewModel : ViewModelBase, IPopupCallback
    {
        readonly ITransactionService transactionService;


        string mod;
        public string MOD
        {
            get => mod;
            set
            {
                mod = value;
                OnPropertyChanged();
            }

        }


        string btcvalue = "0.00020647";
        public string BTCvalue
        {
            get => btcvalue;
            set
            {
                btcvalue = value;
                OnPropertyChanged();
            }

        }


        string payamount = "0.00";
        public string Payamount
        {
            get => payamount;
            set
            {
                payamount = value;
                OnPropertyChanged();
            }

        }

        string availableBalance = "0.13750000";
        public string AvailableBalance
        {
            get => availableBalance;
            set
            {
                availableBalance = value;
                OnPropertyChanged();
            }

        }

        List<Coins> coinList;
        public List<Coins> CoinList
        {
            get => coinList;
            set
            {
                coinList = value;
                OnPropertyChanged();
            }
        }

        Coins selectedFilterValue;
        public Coins SelectedFilterValue
        {
            get => selectedFilterValue;
            set
            {
                selectedFilterValue = value;
                OnPropertyChanged();

                Name = value.Name;
                Logo = value.Logo;
            }
        }




        string logo;
        public string Logo
        {
            get => logo;
            set
            {
                logo = value;
                OnPropertyChanged();
            }

        }




        string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }

        }

        string transactionlogo;
        public string Transactionlogo
        {
            get => transactionlogo;
            set
            {
                transactionlogo = value;
                OnPropertyChanged();
            }

        }

        string coinValue;
        public string CoinValue
        {
            get => coinValue;
            set
            {
                coinValue = value;
                OnPropertyChanged();
            }

        }

        string handlingfee = "2";
        public string Handlingfee
        {
            get => handlingfee;
            set
            {
                handlingfee = value;
                OnPropertyChanged();
            }

        }

        string enteramount;
        public string Enteramount
        {
            get => enteramount;
            set
            {
                enteramount = value;
                OnPropertyChanged();
            }

        }

        string myWalletAddress;
        public string MyWalletAddress
        {
            get => myWalletAddress;
            set
            {
                myWalletAddress = value;
                OnPropertyChanged();
            }

        }


        public ICommand SwapCmd => new AsyncCommand(OnSwapVerification);

        public SwapViewModel(ITransactionService transactionService)
        {
            this.transactionService = transactionService;

        }


        async Task OnSwapVerification()
        {
            if (!IsValidData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await transactionService.SwapVerificationAsync(Enteramount);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            await NavigationService.NavigatePopupAsync<OtpPopupViewModel>();
        }


        bool IsValidData()
        {
            bool isValid = false;
            if (!Enteramount.IsValid())
                ToastService.ShowToast(Constants.EMPTY_SWAP_AMOUNT);
            //else if (!WithdrawAddress.IsValid())
            //    ToastService.ShowToast(Constants.EMPTY_SWAP_ADDRESS);
            //else if (!double.TryParse(Balance, out double availableBalance) || withdrawBalance > availableBalance)
            //    ToastService.ShowToast(Constants.LOW_BALANCE);
            else isValid = true;
            return isValid;

        }



        public SwapViewModel()
        {
            CoinList = new List<Coins>
            {
                new Coins
               {
                   Name="BTC",Logo="Bitcoin"


               },
                 new Coins
               {
                   Name="ETH",Logo="Etherium"


               },
               new Coins
               {
                   Name="LTC",Logo="ltc"

               },

                new Coins
               {
                   Name="NGT",Logo="logo"

               },

                 new Coins
               {
                   Name="USDT",Logo="usdt"

               },



        };
            SelectedFilterValue = CoinList[0];


        }




        public override Task InitializeAsync(object navigationData) => Initialize(navigationData);
        async Task Initialize(object data)
        {
            if (data != null)
            {
                if (data is Coins coins)
                {
                    AvailableBalance = coins.AvailableVolume;

                    CoinValue = coins.Name;
                    Transactionlogo = coins.Logo;
                }
            }
            await Task.CompletedTask;
        }

        public async Task OnSubmit(object data)
        {
            IsBusy = true;
            var response = await transactionService.SwapConfirmationAsync(Enteramount, data.ToString());
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
        }
    }
}
