﻿using System;
using System.Collections.Generic;
using SmartWallet.Module.Withdraw.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using ZXing.Net.Mobile.Forms;

namespace SmartWallet.Module.Withdraw.View
{
    public partial class WithdrawPage : ContentPage
    {


        public WithdrawPage()
        {
            InitializeComponent();
            activityIndicator.On<Android>().SetElevation(12);
        }




        void OnScanQRCode(object sender, EventArgs e)
        {

            var scannerPage = new ZXingScannerPage();
            Navigation.PushAsync(scannerPage);
            scannerPage.OnScanResult += (result) =>
            {
                scannerPage.IsScanning = false;
                ZXing.BarcodeFormat barcodeFormat = result.BarcodeFormat;
                string type = barcodeFormat.ToString();
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PopAsync();
                    (BindingContext as WithdrawViewModel).Toaddress = result.Text;


                });

            };
        }
    }
}



//void OnScanQRCode(object sender, EventArgs e)
//{

//    var scannerPage = new ZXingScannerPage();
//   
//    scannerPage.OnScanResult += (result) =>
//    {
//        scannerPage.IsScanning = false;
//        if (result.BarcodeFormat == ZXing.BarcodeFormat.QR_CODE)
//        {
//            (BindingContext as WithdrawViewModel).Toaddress = result.Text;
//            Navigation.PopModalAsync();
//        }
//    };
//    Navigation.PushAsync(scannerPage);
//}
//    }