﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Alert.ViewModel;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Module.Transactions.Model;
using SmartWallet.Services.Dialog;
using SmartWallet.Services.Transaction;
using SmartWallet.Utils;

namespace SmartWallet.Module.Withdraw.ViewModel
{
    public class WithdrawViewModel : ViewModelBase, IPopupCallback
    {
        readonly ITransactionService transactionService;

        string toaddress;
        public string Toaddress
        {
            get => toaddress;
            set
            {
                toaddress = value;
                OnPropertyChanged();
            }

        }

        string myWalletAddress;
        public string MyWalletAddress
        {
            get => myWalletAddress;
            set
            {
                myWalletAddress = value;
                OnPropertyChanged();
            }

        }


        string availableBalance = "0.0687780";
        public string AvailableBalance
        {
            get => availableBalance;
            set
            {
                availableBalance = value;
                OnPropertyChanged();
            }

        }


        string coinValue = "BTC";
        public string CoinValue
        {
            get => coinValue;
            set
            {
                coinValue = value;
                OnPropertyChanged();
            }

        }

        string fee = "0.0002500000";
        public string Fee
        {
            get => fee;
            set
            {
                fee = value;
                OnPropertyChanged();
            }

        }
        string arrivalAmount = "0.0002500000";
        public string ArrivalAmount
        {
            get => arrivalAmount;
            set
            {
                arrivalAmount = value;
                OnPropertyChanged();
            }

        }

        string amount;
        public string Amount
        {
            get => amount;
            set
            {
                amount = value;
                OnPropertyChanged();
                ChangeArrivalAmount();
            }

        }

        string receivedCoin;
        public string ReceivedCoin
        {
            get => receivedCoin;
            set
            {
                receivedCoin = value;
                OnPropertyChanged();
            }
        }

        public ICommand WithdrawCmd => new AsyncCommand(OnWithdrawVerification);
        public WithdrawViewModel(ITransactionService transactionService)
        {
            this.transactionService = transactionService;
        }

        public override Task InitializeAsync(object navigationData) => Initialize(navigationData);
        async Task Initialize(object data)
        {
            if (data != null)
            {
                if (data is TransactionCoinsDetails coins)
                {
                    AvailableBalance = coins.WalletBalance;
                    Fee = coins.TransactionFees;
                    CoinValue = coins.CoinName;
                    ReceivedCoin = coins.CoinSymbol;
                }
            }
            await Task.CompletedTask;
        }


        void ChangeArrivalAmount()
        {
            if (Amount.IsValid())
            {
                var sum = double.Parse(Amount) - double.Parse(Fee);
                ArrivalAmount = sum.ToString();
            }
        }


        async Task OnWithdrawVerification()
        {
            if (!IsValidData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await transactionService.WithdrawVerificationAsync(Amount, Toaddress, ReceivedCoin);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            await NavigationService.NavigatePopupAsync<OtpPopupViewModel>();
        }

        bool IsValidData()
        {
            bool isValid = false;

            if (!Toaddress.IsValid())
                ToastService.ShowToast(Constants.EMPTY_WITHDRAW_ADDRESS);
            else if (!Amount.IsValid())
                ToastService.ShowToast(Constants.EMPTY_WITHDRAW_AMOUNT);
            //else if (!WithdrawAddress.IsValid())
            //    ToastService.ShowToast(Constants.EMPTY_WITHDRAW_ADDRESS);
            //else if (!double.TryParse(Balance, out double availableBalance) || withdrawBalance > availableBalance)
            //    ToastService.ShowToast(Constants.LOW_BALANCE);
            else isValid = true;
            return isValid;
        }

        public async Task OnSubmit(object data)
        {
            IsBusy = true;
            var response = await transactionService.WithdrawConfirmationAsync(Amount, Toaddress, ReceivedCoin, data.ToString());
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
        }
    }
}