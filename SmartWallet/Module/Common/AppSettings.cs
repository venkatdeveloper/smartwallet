﻿using Xamarin.Essentials;

namespace SmartWallet.Module.Common
{
    public static class AppSettings
    {
        public static bool IsLogin
        {
            get => Preferences.Get(nameof(IsLogin), false);
            set => Preferences.Set(nameof(IsLogin), value);
        }
        public static string Token
        {
            get => Preferences.Get(nameof(Token), string.Empty);
            set => Preferences.Set(nameof(Token), value);
        }
        public static string UserName
        {
            get => Preferences.Get(nameof(UserName), string.Empty);
            set => Preferences.Set(nameof(UserName), value);
        }

        public static string Title
        {
            get => Preferences.Get(nameof(Title), string.Empty);
            set => Preferences.Set(nameof(Title), value);
        }

        public static string Message
        {
            get => Preferences.Get(nameof(Message), string.Empty);
            set => Preferences.Set(nameof(Message), value);
        }

        public static bool PushNotificationEnabled
        {
            get => Preferences.Get(nameof(PushNotificationEnabled), true);
            set => Preferences.Set(nameof(PushNotificationEnabled), value);
        }

        public static bool HasNewNotification
        {
            get => Preferences.Get(nameof(HasNewNotification), false);
            set => Preferences.Set(nameof(HasNewNotification), value);
        }

        public static void Clear()
        {
            IsLogin = false;
            UserName = string.Empty;
            Token = string.Empty;
        }

        public static void ClearNotification()
        {
            Title = string.Empty;
            Message = string.Empty;
            HasNewNotification = false;
        }
    }
}
