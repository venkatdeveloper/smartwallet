﻿using System;
using Autofac;
using SmartWallet.Services.Dialog;
using SmartWallet.Services.Navigation;
using SmartWallet.Services.Request;
using SmartWallet.Module.Startup.ViewModel;
using SmartWallet.Services.Transaction;
using SmartWallet.Services.Account;
using SmartWallet.Services.Authenticate;
using SmartWallet.Module.Root.ViewModel;
using SmartWallet.Module.Register.ViewModel;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Module.Dashboard.ViewModel;
using SmartWallet.Module.Transactions.ViewModel;
using SmartWallet.Module.ForgotPassword.ViewModal;
using SmartWallet.Module.Market.ViewModel;
using SmartWallet.Module.MasterStaking.ViewModel;
using SmartWallet.Module.Profile.ViewModel;
using SmartWallet.Module.Assets.ViewModel;
using SmartWallet.Module.About.ViewModel;
using SmartWallet.Module.Affiliate.ViewModel;
using SmartWallet.Module.ChangePassword.ViewModel;
using SmartWallet.Module.Community.ViewModel;
using SmartWallet.Module.Deposit.ViewModel;
using SmartWallet.Module.GenerationRevenue.ViewModel;
using SmartWallet.Module.KYC.ViewModel;
using SmartWallet.Module.PersonalStaking.ViewModel;
using SmartWallet.Module.Revenue.ViewModel;
using SmartWallet.Module.TotalRevenue.ViewModel;
using SmartWallet.Module.Withdraw.ViewModel;
using SmartWallet.Module.Swap.ViewModel;
using SmartWallet.Module.SplashScreen.ViewModel;
using SmartWallet.Module.StakingRecord.ViewModel;
using SmartWallet.Services.Support;
using SmartWallet.Module.Phrase.ViewModel;
using SmartWallet.Services.Market;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Module.Alert.ViewModel;
using SmartWallet.Services.Phrase;

namespace SmartWallet.Module.Common
{
    public class Locator
    {
        static Locator instance;
        public static Locator Instance => instance ?? (instance = new Locator());

        readonly IContainer builder;

        Locator()
        {
            var containerBuilder = new ContainerBuilder();

            //Register Services
            containerBuilder.RegisterType<NavigationService>().As<INavigationService>().SingleInstance();
            containerBuilder.RegisterType<DialogService>().As<IDialogService>().SingleInstance();
            containerBuilder.RegisterType<RequestService>().As<IRequestService>().SingleInstance();
            containerBuilder.RegisterType<AuthenticateService>().As<IAuthenticateService>().SingleInstance();
            containerBuilder.RegisterType<PhraseServices>().As<IPhraseServices>().SingleInstance();
            containerBuilder.RegisterType<TransactionService>().As<ITransactionService>().SingleInstance();
            containerBuilder.RegisterType<AccountService>().As<IAccountService>().SingleInstance();
            containerBuilder.RegisterType<SupportService>().As<ISupportService>().SingleInstance();
            containerBuilder.RegisterType<MarketService>().As<IMarketService>().SingleInstance();


            //Register ViewModels
            containerBuilder.RegisterType<StartupViewModel>();
            containerBuilder.RegisterType<AboutViewModel>();
            containerBuilder.RegisterType<RootViewModel>();
            containerBuilder.RegisterType<RegisterViewModal>();
            containerBuilder.RegisterType<ForgotPasswordViewModel>();
            containerBuilder.RegisterType<MarketViewModel>();
            containerBuilder.RegisterType<AssetViewModel>();
            containerBuilder.RegisterType<LoginViewModel>();
            containerBuilder.RegisterType<DashboardViewModel>();
            containerBuilder.RegisterType<MasterStakingViewModel>();
            containerBuilder.RegisterType<ProfileViewModel>();
            containerBuilder.RegisterType<AffiliateViewModel>();
            containerBuilder.RegisterType<TransactionViewModel>();
            containerBuilder.RegisterType<ChangePasswordViewModel>();
            containerBuilder.RegisterType<CommunityViewModel>();
            containerBuilder.RegisterType<DepositViewModel>();
            containerBuilder.RegisterType<GenerationRevenueViewModel>();
            containerBuilder.RegisterType<KYCViewModel>();
            containerBuilder.RegisterType<PersonalStakingViewModel>();
            containerBuilder.RegisterType<RevenueViewModel>();
            containerBuilder.RegisterType<TotalRevenueViewModel>();
            containerBuilder.RegisterType<TransactionViewModel>();
            containerBuilder.RegisterType<WithdrawViewModel>();
            containerBuilder.RegisterType<SwapViewModel>();
            containerBuilder.RegisterType<SplashScreenViewModel>();
            containerBuilder.RegisterType<StakingRecordViewModel>();
            containerBuilder.RegisterType<ForgetUserIdViewModel>();
            containerBuilder.RegisterType<PhraseViewModel>();
            containerBuilder.RegisterType<CountryViewModel>();
            containerBuilder.RegisterType<OtpPopupViewModel>();
            builder = containerBuilder.Build();
        }

        public T Resolve<T>() => builder.Resolve<T>(); //for creating instance

        public object Resolve(Type type) => builder.Resolve(type); //for creating instance
    }
}
