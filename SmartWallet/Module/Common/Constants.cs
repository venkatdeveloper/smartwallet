﻿namespace SmartWallet.Module.Common
{
    public sealed class Constants
    {
        Constants() { }

        internal const string APP_NAME = "SmartWallet";
        internal const string BASE_URL = "https://nextgenwallet.io/WalletAPI/";
        //internal const string TranslateUrl = "http://mymemory.translated.net/api/";
        //Router


        //current ImplementationApi
        internal const string VERIFY_USER_ID_API = "checkusername.asp";
        internal const string LOGIN_API = "MemberLogin1.asp";
        internal const string SIGNUP_SMS_API = "send_sms_signup.asp";
        internal const string REGISTRATION_FIRST_STEP_API = "Reg.asp";
        internal const string REGISTRATION_PERSONAL_INFO_API = "Reg_personal.asp";
        internal const string REGISTRATION_LOGIN_PWD_API = "Reg_login_pwd.asp";
        internal const string REGISTRATION_TRADING_PWD_API = "Reg_trading_pwd.asp";
        internal const string EMAIL_VERIFICATION_API = "email_verification_code.asp";
        internal const string EMAIL_VERIFICATION_SUBMIT_API = "email_verification_submit.asp";
        internal const string GET_MNEMONIC_PHRASE_API = "get_mnemonic.asp";
        internal const string SHOW_MNEMONIC_PHRASE_API = "show_mnemonic.asp";
        internal const string CREATE_MNEMONIC_PHRASE_API = "create_mnemonic.asp";
        internal const string VERIFY_MNEMONIC_PHRASE_API = "verify_mnemonic.asp";
        internal const string FORGOT_USER_ID_BY_EMAIL_API = "ForgotUserID_email.asp";
        internal const string FORGOT_PASSWORD_BY_EMAIL_API = "ForgotPassword_email.asp";
        internal const string FORGOT_USER_ID_BY_MNEMONIC_API = "ForgotUserID_mnemonic.asp";
        internal const string FORGOT_ForgotPassword_BY_MNEMONIC_API = "ForgotPassword_mnemonic.asp";
        internal const string GET_DASHBOARD_DETAILS_API = "GetDashboardDetails.asp";
        internal const string GET_INDIVIDUAL_COIN_DETAILS_API = "GetRateCrypto.asp";
        internal const string GET_TRANSACTIONS_API = "Transactions.asp";
        internal const string DEPOSIT_API = "deposit.asp";
        internal const string WITHDRAWAL_REQUEST_API = "WidthdrawalRequest.asp";
        internal const string WITHDRAWAL_CONFIRM_API = "WidthdrawalConfirm.asp";
        internal const string WITHDRAWAL_REPORT_API = "WithdrawalReport.asp";
        internal const string SWAP_REQUEST_API = "swap.asp";
        internal const string SWAP_CONFIRM_API = "swapconfirm.asp";
        internal const string SWAP_REPORT_API = "SwapReport.asp";
        internal const string GET_PROFILE_DETAILS_API = "MyProfile.asp";
        internal const string CHANGEPASSWORD_API = "ChangePassword.asp";
        internal const string CHANGETRADEPASSWORD_API = "ChangetPassword.asp";
        internal const string CHANGE_PUSH_NOTIFICATION_STATUS_API = "pushnotificationstatus.asp";        
        internal const string GET_KYC_API = "GetKYC.asp";
        internal const string UPDATE_KYC_API = "uploadimagekyc.asp";
        internal const string GET_AFFILIATE_LINK_API = "getaffiliatelink.asp";
        internal const string BUY_HISTORY_API = "GetPurchasePortfolio.asp";
        internal const string BUY_MASTER_STACKING_CONFIRM_API = "buycontract.asp";
        internal const string BUY_MASTER_STACKING_API = "buycontractConfirm.asp";
        internal const string DEPOSIT_HISTORY_API = "deposithistory.asp";
        internal const string CREATE_NEW_CHANGE_REQUEST_API = "support.asp";
        internal const string CHANGE_REQUEST_REPORT_API = "supportreport.asp";
        internal const string GET_COUNTRY_LIST_API = "country.asp";
        internal const string LOGIN_HISTORY_API = "loginhostory.asp";
        internal const string GET_NG_DETAILS_API = "NGDashboardDetails.asp";
        internal const string DIRECT_SPONSORS_REPORT_API = "directsponsorreport.asp";
        internal const string GET_COMMUNITY_API = "directsponsorreport.asp";
        internal const string GET_REVENUE_API = "GetIncomeSummary.asp";
        internal const string GET_PERSONALREVENUE_API = "GetROI.asp";
        internal const string GET_GENERALREVENUE_API = "GetLevel.asp";


        ////current Implementation api


        //internal const string REGISTER_API = "member_registration.asp";
        //internal const string FORGOT_USERID_API = "forgot_used_id.asp";
        //internal const string VERIFY_REFERRAL_CODE = "verify_referral_code.asp";
        //internal const string CHECK_VERIFY_CODE = "check_verification_code.asp";

        //internal const string SEND_COIN_API = "";
        //internal const string REQUEST_COIN_API = "";
        //internal const string TRANSACTION_API = "";
        //internal const string GET_COIN_DETAILS_API = "";

        //internal const string FORGOTPASSWORD_API = "forgot_password.asp";

        //internal const string CHANGETRADEPASSWORD_API = "ChangeTradePassword.asp";

        //internal const string WITHDRAW_API = "withdraw.asp";
        //internal const string TRANSACTION_HISTORY_API = "TransactionsHistory.asp";
        //internal const string SWAP_API = "swap.asp";
        //internal const string STAKING_DETAILS_API = "StackingDetails.asp";
        //internal const string MASTERSTAKING_API = "ActivateMasterStaking.asp";
        //internal const string MASTERNODES_API = "GetMasterNodes.asp";
        //internal const string REVENUEDETAILS_API = "RevenueDetails.asp";
        //internal const string REVENUEHISTORY_API = "RevenueHistory.asp";
        //internal const string STAKINGHISTORY_API = "StakingHistory.asp";

        //internal const string GET_VERIFY_CODE_API = "GetVerifyCode.asp";
        //internal const string GET_REG_VERIFY_CODE_API = "GetRegVerifyCode.asp";
        //internal const string AFFILIATE_LINK_API = "GetReferenceCode.asp";
        //internal const string COUNTRY_LIST_API = "CountryList.asp";
        //internal const string PUSH_NOTIFICATION_API = "EnablePushNotifications.asp";
        //internal const string REQUESTCOIN_API = "EditProfile.asp";
        //internal const string ROOT_API = "DashboardDetails.asp";

        ////internal const string CHANGEREQUEST_API = "changeRequest.php";
        ////internal const string CHANGEREQUEST_HISTORY_API = "crList.php";
        ////internal const string TRANSACTION_API = "transaction.php";
        ////internal const string TRANSACTION_REDEEM_API = "transactionRedeem.php";
        ////internal const string TRANSACTION_HISTORY_API = "transactionDetail.php";
        //internal const string PROFILE_API = "GetProfile.asp";
        ////internal const string VENDOR_LIST_API = "vendorList.php";
        ////internal const string CUSTOMER_QR_ID_API = "customeridQR.php";
        //internal const string CREATE_PIN_API = "";
        //internal const string VERIFY_PIN_API = "";
        //internal const string CHANGE_PIN_API = "";

        //Message
        internal const string OK = "Ok";
        internal const string CANCEL = "Cancel";
        internal const string LOGOUT = "Logout";
        internal const string SUCCESS = "Success";
        internal const string FAILURE = "Failure";
        internal const string DELETE = "Delete";
        internal const string ATTACHMENTS = "Attachments";
        internal const string CHECK_CONNECTIVITY = "Please check your internet connectivity";
        internal const string CONFIRMATION = "Confirmation";
        internal const string SIGN_OUT_CONFIRMATION_MESSAGE = "Are you sure you want to sign out of your account";
        internal const string INVALID_CERDENTIALS = "Invalid credentials";
        internal const string EMPTY_USERNAME = "Enter the user name";
        internal const string EMPTY_FULLNAME = "Enter the fullname";
        internal const string EMPTY_PASSWORD = "Enter the password";
        internal const string EMPTY_MOBILENUMBER = "Enter the mobile number";
        internal const string EMPTY_CHECKBOX = "Agree the terms and privacy policy";
        internal const string EMPTY_CONFIRMPASSWORD = "Enter the confirm password";
        internal const string EMPTY_VERIFICATIONCODE = "Enter the verification code";
        internal const string EMPTY_MOBILE_NUMBER = "Enter the mobile number";
        internal const string PASSWORD_MISMATCH = "Enter the correct password";
        internal const string EMPTY_SWAP_AMOUNT = "Enter the swap amount";
        internal const string EMPTY_EMAIL = "Enter the Email";
        internal const string EMPTY_WITHDRAW_ADDRESS = "Enter the withdraw address";
        internal const string EMPTY_WITHDRAW_AMOUNT = "Enter the withdraw amount";
        internal const string EMPTY_IMAGE = "Upload the image";
        internal const string INVALID_EMAIL = "Enter the valid Email";
        internal const string EMPTY_OTP = "Please enter the otp";
        internal const string CONFIRM_PIN = "Enter Confirm Password";
        internal const string CREATE_PIN = "Create your 4 digit pin";
        internal const string ENTER_PIN = "Enter your 4 digit pin";
        internal const string ENTER_OLD_PASSWORD = "Enter your old Password";
        internal const string ENTER_NEW_PASSWORD = "Enter your new Password";
        internal const string ENTER_OLD_PIN = "Enter your old Pin";
        internal const string ENTER_NEW_PIN = "Enter your new Pin";
        internal const string TOKEN_EXPIRED = "Your token has expired. Please Login to continue";
        internal const string WARNING = "WARNING";
        internal const string PHOTO_CHOOSEN_TITLE= "Choose photo from";
        internal const string GALLERY = "Gallery";
        internal const string CAMERA = "Camera";
        internal const string EMPTY_GOVTID = "Enter the govtId";
        internal const string EMPTY_REFERCODE = "Enter the refer code";
        internal const string EMPTY_STAKING_AMOUNT = "Enter the staking amount";
        internal const string REENTER_PASSWORD = "Re-enter the password";
        internal const string ENTER_TRADE_PASSWORD= "Enter the trade password";
        internal const string REENTER_TRADE_PASSWORD = "Re-enter the trade password";

        //Formatter String
        internal const string DD_MMMM_YYYY = "dd MMMM yyyy";

        //HTTP KEY
        internal const string AUTHORIZATION_KEY = "Authorization";

        internal const string HIDE_MASTER_KEY = "com.multicoin.hide_master_key";

        internal const string AUTHENTICATION_TOKEN_END_POINT = "https://api.cognitive.microsoft.com/sts/v1.0";
        public static readonly string TextTranslatorEndpoint = "https://api.microsofttranslator.com/v2/http.svc/translate";

        
    }
}