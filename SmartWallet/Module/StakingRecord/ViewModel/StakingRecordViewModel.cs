﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Market;
using SmartWallet.Utils;

namespace SmartWallet.Module.StakingRecord.ViewModel
{
    public class StakingRecordViewModel : ViewModelBase
    {

        readonly IMarketService marketService;


        List<BuyHistoryItems> stakingRecordList;
        public List<BuyHistoryItems> StakingRecordList
        {
            get => stakingRecordList;
            set
            {
                stakingRecordList = value;
                OnPropertyChanged();
            }
        }

        bool canShowEmpty;
        public bool CanShowEmpty
        {
            get => canShowEmpty;
            set
            {
                canShowEmpty = value;
                OnPropertyChanged();
            }
        }

        public StakingRecordViewModel(IMarketService marketService)
        {
            this.marketService = marketService;
        }

        public override Task InitializeAsync(object navigationData) => InitialSetup();

        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var res = await marketService.StakingHistoryAsync();
            IsBusy = false;
            if (!res.IsSuccess())
            {
                ToastService.ShowToast(res.Message);
                return;
            }
            StakingRecordList = res.Items;
            CanShowEmpty = StakingRecordList == null || StakingRecordList.Count < 0;
        }
    }
}
