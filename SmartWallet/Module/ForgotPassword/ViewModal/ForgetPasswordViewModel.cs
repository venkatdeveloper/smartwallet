﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Module.Phrase.ViewModel;
using SmartWallet.Services.Account;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;

namespace SmartWallet.Module.ForgotPassword.ViewModal
{
    public class ForgotPasswordViewModel : ViewModelBase
    {
        readonly IAuthenticateService authendicationServices;
        //string userName;
        //public string UserName
        //{
        //    get => userName;
        //    set
        //    {
        //        if (userName != value)
        //        {
        //            userName = value;
        //            OnPropertyChanged();
        //        }
        //    }
        //}
        bool optionsStack = true;
        public bool OptionsStack
        {
            get => optionsStack;
            set
            {
                optionsStack = value;
                OnPropertyChanged();
            }
        }
        bool cofirmationMessageStack;
        public bool CofirmationMessageStack
        {
            get => cofirmationMessageStack;
            set
            {
                cofirmationMessageStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameEntryStack;
        public bool UserNameEntryStack
        {
            get => userNameEntryStack;
            set
            {
                userNameEntryStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseStack;
        public bool PhraseStack
        {
            get => phraseStack;
            set
            {
                phraseStack = value;
                OnPropertyChanged();
            }
        }
        bool showOptions;
        public bool ShowOptions
        {
            get => showOptions;
            set
            {
                showOptions = value;
                OnPropertyChanged();
            }
        }
        string userID = string.Empty;
        public string UserID
        {
            get => userID;
            set
            {
                userID = value;
                OnPropertyChanged();
                if (UserID.IsValid())
                {
                    UserImage = "right_round";
                    RotationValue = "0";
                }
                else
                {
                    UserImage = "clear";
                    RotationValue = "45";
                }
            }
        }
        string userImage;
        public string UserImage
        {
            get => userImage;
            set
            {
                userImage = value;
                OnPropertyChanged();
            }
        }
        string emailID = string.Empty;
        public string EmailID
        {
            get => emailID;
            set
            {
                emailID = value;
                OnPropertyChanged();
                if (EmailID.IsValidMail())
                {
                    EmailImage = "right_round";
                    RotationValue = "0";
                }
                else
                {
                    EmailImage = "clear";
                    RotationValue = "45";
                }
            }
        }
        string emailImage;
        public string EmailImage
        {
            get => emailImage;
            set
            {
                emailImage = value;
                OnPropertyChanged();
            }
        }
        string rotationValue;
        public string RotationValue
        {
            get => rotationValue;
            set
            {
                rotationValue = value;
                OnPropertyChanged();
            }
        }
        string phraseWords;
        public string PhraseWords
        {
            get => phraseWords;
            set
            {
                phraseWords = value;
                OnPropertyChanged();
            }
        }

        // public ICommand ForgotPasswordCmd => new AsyncCommand(OnForGotPassword);

        public ICommand ClearCmd => new AsyncCommand(OnClear);
        public ICommand ShowOptionsCmd => new AsyncCommand(OnShowOptions);
        public ICommand ShowUserStackCmd => new AsyncCommand(OnShowUserStack);
        public ICommand GotoLoginCmd => new AsyncCommand(GotoLogin);
        public ICommand ResendEmailCmd => new AsyncCommand(ResendEmail);
        public ForgotPasswordViewModel(IAuthenticateService authendicationServices)
        {
            this.authendicationServices = authendicationServices;
        }
        //public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);

        //async Task InitialSetup(object data)
        //{

        //}
        async Task OnShowOptions()
        {
            if (ShowOptions)
                ShowOptions = false;
            else
                ShowOptions = true;
            await Task.CompletedTask;
        }

        async Task OnForGotPassword()
        {
            if (!IsValidate()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            if (PhraseStack)
            {
                IsBusy = true;
                var response = await authendicationServices.ForgotPasswordMneMonicAsync(PhraseWords, UserID);
                IsBusy = false;
                if (!response.IsSuccess())
                {
                    ToastService.ShowToast(response.Message);
                    return;
                }
                ToastService.ShowToast(response.Message);
                OptionsStack = false;
                CofirmationMessageStack = true;
            }
            else
            {
                IsBusy = true;
                var response = await authendicationServices.ForgotPasswordEmailAsync(EmailID, UserID);
                IsBusy = false;
                if (!response.IsSuccess())
                {
                    ToastService.ShowToast(response.Message);
                    return;
                }
                ToastService.ShowToast(response.Message);
                OptionsStack = false;
                PhraseStack = false;
                CofirmationMessageStack = true;
            }
        }
        async Task GotoLogin()
        {
            await NavigationService.NavigateBackAsync();
        }
        async Task ResendEmail()
        {
            await OnForGotPassword();
            ToastService.ShowToast("Message Resended");
        }
        async Task OnClear(object data)
        {
            if (data.ToString() == "User")
            {
                UserID = string.Empty;
            }
            else
            {
                EmailID = string.Empty;
            }
            await Task.CompletedTask;
        }
        async Task OnShowUserStack(object data)
        {
            if (data.ToString() == "User")
            {
                await OnForGotPassword();
            }
            else
            {
                OptionsStack = false;
                PhraseStack = true;
            }
            await Task.CompletedTask;
        }
        bool IsValidate()
        {
            var isValid = false;
            if (!UserID.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            if (!EmailID.IsValidMail())
                ToastService.ShowToast(Constants.INVALID_EMAIL);
            else
                isValid = true;
            return isValid;
        }        
    }
}


