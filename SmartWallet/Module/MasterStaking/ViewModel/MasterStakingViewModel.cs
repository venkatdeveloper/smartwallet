﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
using SmartWallet.Module.MasterStaking.Model;
using SmartWallet.Services.Account;
using SmartWallet.Services.Dialog;
using SmartWallet.Services.Market;
using SmartWallet.Services.Transaction;
using SmartWallet.Utils;

namespace SmartWallet.Module.MasterStaking.ViewModel
{
    public class MasterStakingViewModel : ViewModelBase
    {
        readonly IMarketService marketService;
        readonly IAccountService accountService;
        readonly IDialogService dialogService;
        List<MasterStakingCoinModel> coinsList;
        public List<MasterStakingCoinModel> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }

        MasterStakingCoinModel selectedCoin;
        public MasterStakingCoinModel SelectedCoin
        {

            get => selectedCoin;
            set
            {

                selectedCoin = value;
                OnPropertyChanged();
                var ignore = Test(value);
            }
        }


        string selectedLogo;
        public string SelectedLogo
        {

            get => selectedLogo;
            set
            {
                selectedLogo = value;
                OnPropertyChanged();


            }
        }

        string selectedName;
        public string SelectedName
        {

            get => selectedName;
            set
            {
                selectedName = value;
                OnPropertyChanged();


            }
        }





        string btcvalue = "0.0000000";
        public string BTCvalue
        {

            get => btcvalue;
            set
            {
                btcvalue = value;
                OnPropertyChanged();


            }
        }


        string amount;
        public string Amount
        {

            get => amount;
            set
            {
                amount = value;
                OnPropertyChanged();


            }
        }



        string stakingAmount;
        public string StakingAmount
        {

            get => stakingAmount;
            set
            {
                stakingAmount = value;
                OnPropertyChanged();


            }
        }

        string maxamount = "0.00";
        public string MaxAmount
        {

            get => maxamount;
            set
            {
                maxamount = value;
                OnPropertyChanged();


            }
        }




        public ICommand ConfirmCmd => new AsyncCommand(OnConfirm);

        public MasterStakingViewModel(IMarketService marketService, IAccountService accountService, IDialogService dialogService)
        {
            this.marketService = marketService;
            this.accountService = accountService;
            this.dialogService = dialogService;
        }

        public override Task InitializeAsync(object navigationData) => SetupMenuData();

        async Task OnConfirm()
        {
            if (!IsValidData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.MasterStakingAsync(StakingAmount, SelectedCoin.Name);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            var res = await dialogService.ShowConfimationAsync("Next Gen", response.Message, "Confirm", Constants.CANCEL);
            if (res)
                await StakingPurchaseConfirmation();
        }

        async Task StakingPurchaseConfirmation()
        {
            if (!IsValidData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.MasterStakingConfirmationAsync(StakingAmount, SelectedCoin.Name);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
        }


        bool IsValidData()
        {
            bool isValid = false;
            if (!StakingAmount.IsValid())
                ToastService.ShowToast(Constants.EMPTY_STAKING_AMOUNT);
            else
                isValid = true;
            return isValid;

        }
        async Task SetupMenuData()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await accountService.DashboardDetailsAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            CoinsList = new List<MasterStakingCoinModel>
           {
               new MasterStakingCoinModel
               {
                   Name="BTC",Logo="Bitcoin",AvailableVolume = response.Btc,MarketPercentage = response.Btc24Hr,Rate = response.BtcUsd
               },
               new MasterStakingCoinModel
               {
                   Name="ETH",Logo="Etherium",AvailableVolume = response.Eth,MarketPercentage = response.Eth24Hr,Rate = response.EthUsd
               },
               new MasterStakingCoinModel
               {
                   Name="LTC",Logo="ltc",AvailableVolume = response.Ltc,MarketPercentage = response.Ltc24Hr,Rate = response.LtcUsd
               },
                new MasterStakingCoinModel
               {
                   Name="NGT",Logo="logo",AvailableVolume = response.Ngt,MarketPercentage = response.Ngt24Hr,Rate = response.NgtUsd
               },

                 new MasterStakingCoinModel
               {
                   Name="USDT",Logo="usdt",AvailableVolume = response.Usdt,MarketPercentage = response.Usdt24Hr,Rate = response.UsdtUsd
               },

           };
            SelectedCoin = CoinsList[0];
        }




        async Task Test(MasterStakingCoinModel coins)
        {
            SelectedName = coins.Name;
            SelectedLogo = coins.Logo;
            BTCvalue = coins.Rate;
            Amount = coins.AvailableVolume + " " + coins.Name;
            await Task.CompletedTask;
        }
    }
}

