﻿using System;
namespace SmartWallet.Module.MasterStaking.Model
{
    public class MasterStakingCoinModel
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string AvailableVolume { get; set; }
        public string MarketPercentage { get; set; }
        public string Rate { get; set; }
        public MasterStakingCoinModel()
        {
        }
    }
}
