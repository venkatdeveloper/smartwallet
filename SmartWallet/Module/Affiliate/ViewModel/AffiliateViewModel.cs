﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Services.Account;
using SmartWallet.Services.ScreenShot;
using SmartWallet.Utils;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.TizenSpecific;

namespace SmartWallet.Module.Affiliate.ViewModel
{
    public class AffiliateViewModel : ViewModelBase
    {
        readonly IAccountService accountService;
        string qrCode = AppSettings.UserName;
        public string QrCode
        {
            get => qrCode;
            set
            {
                qrCode = value;
                OnPropertyChanged();
            }
        }



        string username = AppSettings.UserName;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }

        }



        public AffiliateViewModel(IAccountService accountService)
        {
            this.accountService = accountService;
        }
        public override Task InitializeAsync(object navigationData) => InitialSetup();
        async Task InitialSetup()
        {
            IsBusy = true;
            var response = await accountService.GetAffiliateLinkAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            QrCode = response.Qstring;
        }

       // qixiangjason @gmail.com


        public ICommand GotoSharefriendscmd => new AsyncCommand(OnSharefriends);

        async Task OnSharefriends()
        {
            var byteCode = await DependencyService.Get<IScreenshotService>().Capture();
            var backingFile = System.IO.Path.Combine(Xamarin.Essentials.FileSystem.AppDataDirectory, "count.png");

            //await Share.RequestAsync(new ShareFileRequest
            //{
            //    File = new ShareFile(backingFile),
            //    Title = "Share"
            //});
            await Share.RequestAsync(new ShareTextRequest { Text = QrCode, Title = "Referral" });

        }
    }
}
