﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmartWallet.Module.Affiliate.View
{
    public partial class AffiliatePage : ContentPage
    {
        public AffiliatePage()
        {
            InitializeComponent();

            qrCodeImageView.BarcodeOptions.Width = 300;
            qrCodeImageView.BarcodeOptions.Height = 300;
            

        }
    }
}
