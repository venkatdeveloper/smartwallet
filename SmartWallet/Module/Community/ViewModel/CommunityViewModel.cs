﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Market;
using SmartWallet.Utils;

namespace SmartWallet.Module.Community.ViewModel
{
    public class CommunityViewModel : ViewModelBase
    {
        readonly IMarketService marketService;

        string totalCommunityStaking = "0.00";
        public string TotalCommunityStaking
        {
            get => totalCommunityStaking;

            set
            {
                totalCommunityStaking = value;
                OnPropertyChanged();
            }
        }




        string directlink = "0.00";
        public string Directlink
        {
            get => directlink;

            set
            {
                directlink = value;
                OnPropertyChanged();
            }
        }

        string communitylink = "0.00";
        public string Communitylink
        {
            get => communitylink;

            set
            {
                communitylink = value;
                OnPropertyChanged();
            }
        }


        string searchbar;
        public string Searchbar
        {
            get => searchbar;
            set
            {
                searchbar = value;
                OnPropertyChanged();
            }

        }

        List<CommunityMembers> communityList;
        public List<CommunityMembers> CommunityList
        {
            get => communityList;
            set
            {
                communityList = value;
                OnPropertyChanged();
            }
        }

        bool canShowEmpty;
        public bool CanShowEmpty
        {
            get => canShowEmpty;
            set
            {
                canShowEmpty = value;
                OnPropertyChanged();
            }
        }

        public CommunityViewModel(IMarketService marketService)
        {
            this.marketService = marketService;
        }


        public override Task InitializeAsync(object navigationData) => Setup();

        async Task Setup()
        {
            await InitialSetup();
            await CommunityDetails();
        }

        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.StakingDetailsAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            Directlink = response.PersonalStatcking;
            Communitylink = response.TeamStatcking;
            TotalCommunityStaking = response.TeamStatcking;
        }

        async Task CommunityDetails()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.GetCommunityAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            CommunityList = response.UsersList;
            CanShowEmpty = CommunityList == null;// || CommunityList.Count < 0;
        }
    }
}
