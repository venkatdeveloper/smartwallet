﻿using System;
using System.Windows.Input;
using SmartWallet.Module.Common;
using SmartWallet.Module.Market.ViewModel;
using SmartWallet.Utils;
using System.Threading.Tasks;
using SmartWallet.Module.Affiliate.ViewModel;
using SmartWallet.Module.KYC.ViewModel;
using SmartWallet.Module.About.ViewModel;
using SmartWallet.Module.ChangePassword.ViewModel;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Services.Account;
using SmartWallet.Extension;
using SmartWallet.Services.Dialog;
using SmartWallet.Services.Authenticate;
using SmartWallet.Module.Alert.ViewModel;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Module.Phrase.ViewModel;

namespace SmartWallet.Module.Profile.ViewModel
{
    public class ProfileViewModel : ViewModelBase, IPopupCallback
    {
        readonly IAccountService accountService;
        readonly IDialogService dialogService;
        readonly IAuthenticateService authenticateService;

        string username;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }

        }

        string phoneNumber;
        public string PhoneNumber
        {
            get => phoneNumber;
            set
            {
                phoneNumber = value;
                OnPropertyChanged();
            }
        }
        string emailId;
        public string EmailId
        {
            get => emailId;
            set
            {
                emailId = value;
                OnPropertyChanged();
            }
        }

        //string countryCode;
        //public string CountryCode
        //{
        //    get => countryCode;
        //    set
        //    {
        //        countryCode = value;
        //        OnPropertyChanged();
        //    }

        //}


        string kycStatus;
        public string KYCStatus
        {
            get => kycStatus;
            set
            {
                kycStatus = value;
                OnPropertyChanged();
            }

        }

        bool pushNotificationEnabled;
        public bool PushNotificationEnabled
        {
            get => pushNotificationEnabled;
            set
            {
                if (pushNotificationEnabled != value)
                {
                    pushNotificationEnabled = value;
                    OnPropertyChanged();
                    _ = PushNotificationStatusChanged();
                }
            }
        }




        bool canshowUserCenter;
        public bool CanshowUserCenter
        {
            get => canshowUserCenter;
            set
            {
                canshowUserCenter = value;
                OnPropertyChanged();
            }
        }

        bool canshowSecurityCenter;
        public bool CanshowSecurityCenter
        {
            get => canshowSecurityCenter;
            set
            {
                canshowSecurityCenter = value;
                OnPropertyChanged();
            }
        }

        bool canshowSettingsCenter;
        public bool CanshowSettingsCenter
        {
            get => canshowSettingsCenter;
            set
            {
                canshowSettingsCenter = value;
                OnPropertyChanged();
            }
        }

        string securityImageSource = "whitearrow_right";
        public string SecurityImageSource
        {
            get => securityImageSource;
            set
            {
                securityImageSource = value;
                OnPropertyChanged();
            }
        }

        public ICommand ShowSecurityCenterCmd => new AsyncCommand(ShowSecurityCenter);
        public ICommand ShowAuthendicationCmd => new AsyncCommand(ShowAuthendication);
        public ICommand VerifyEmailCmd => new AsyncCommand(OnEmailVerify);
        public ICommand GotoAffiliatecmd => new AsyncCommand(OnAffiliate);
        public ICommand GotoChangepasswordcmd => new AsyncCommand(OnChangepassword);
        public ICommand GotoTradepasswordcmd => new AsyncCommand(OnTradepassword);
        public ICommand GotoLogincmd => new AsyncCommand(OnLogin);
        public ICommand PhraseCmd => new AsyncCommand(OnPhrase);




        public ProfileViewModel(IAccountService accountService, IDialogService dialogService, IAuthenticateService authenticateService)
        {
            this.accountService = accountService;
            this.dialogService = dialogService;
            this.authenticateService = authenticateService;
        }


        public override Task InitializeAsync(object navigationData) => InitialSetup();

        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await accountService.GetProfileAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            Username = response.Fullname;
            PhoneNumber = $"+{response.Countrycode} {response.Mobile}";
            EmailId = response.Email;
            PushNotificationEnabled = bool.Parse(response.Pushnotificationstatus);
            //KYCStatus = response.KYCStatus;

        }
        async Task OnAffiliate() => await NavigationService.NavigateToAsync<AffiliateViewModel>();

        async Task OnTradepassword() => await NavigationService.NavigateToAsync<ChangePasswordViewModel>("Modify Trade");

        async Task OnChangepassword() => await NavigationService.NavigateToAsync<ChangePasswordViewModel>("Modify Login");

        async Task OnLogin()
        {
            var res = await dialogService.ShowConfimationAsync("Next Gen", Constants.SIGN_OUT_CONFIRMATION_MESSAGE, Constants.OK, Constants.CANCEL);
            if (res)
            {
                AppSettings.Clear();
                await NavigationService.NavigateToAsync<LoginViewModel>();
            }
        }

        async Task ShowSecurityCenter()
        {
            if (CanshowSecurityCenter)
            {
                CanshowSecurityCenter = false;
                SecurityImageSource = "whitearrow_right";
            }
            else
            {
                CanshowSecurityCenter = true;
                SecurityImageSource = "whitearrow_down";
            }
            await Task.CompletedTask;
        }

        async Task ShowAuthendication()
        {
            await NavigationService.NavigateToAsync<KYCViewModel>();
        }

        async Task OnPhrase()
        {
            await NavigationService.NavigateToAsync<PhraseViewModel>();
        }

        async Task OnEmailVerify()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await authenticateService.EmailVerificationAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
            await NavigationService.NavigatePopupAsync<OtpPopupViewModel>();
        }

        public async Task OnSubmit(object data)
        {
            IsBusy = true;
            var response = await authenticateService.SubmitEmailVerificationAsync(data.ToString());
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
        }

        async Task PushNotificationStatusChanged()
        {
            var response = await accountService.ChangePushNotificationAsync(PushNotificationEnabled.ToString());
            ToastService.ShowToast(response.Message);
        }
    }
}
