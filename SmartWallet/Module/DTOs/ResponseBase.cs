﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class ResponseBase
    {
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("status_code")]
        public int StatusCode { get; set; }


    }





}
