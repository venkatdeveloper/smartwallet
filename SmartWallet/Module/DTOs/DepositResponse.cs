﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class DepositResponse : ResponseBase
    {
        [JsonProperty("Address")]
        public string Address { get; set; }
    }
}
