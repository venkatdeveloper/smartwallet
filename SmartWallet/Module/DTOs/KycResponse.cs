﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class KycResponse : ResponseBase
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("IDcardFrontURL")]
        public string IDcardFrontUrl { get; set; }

        [JsonProperty("IDcardBackURL")]
        public string IDcardBackUrl { get; set; }

        [JsonProperty("SelfiURL")]
        public string SelfiUrl { get; set; }

        [JsonProperty("Status")]
        public string KycStatus { get; set; }        
    }
}
