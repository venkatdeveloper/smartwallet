﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class CountryListResponse:ResponseBase
    {
        [JsonProperty("items")]
        public List<Country> CountryList { get; set; }
    }
    public class Country
    {
        [JsonProperty("Countryname")]
        public string Countryname { get; set; }

        [JsonProperty("SortName")]
        public string SortName { get; set; }

        [JsonProperty("PhoneCode")]        
        public string PhoneCode { get; set; }
    }
}
