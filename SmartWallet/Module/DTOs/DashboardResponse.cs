﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class DashboardResponse : ResponseBase
    {
        [JsonProperty("fullname")]
        public string Fullname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("totalasset")]
        public string Totalasset { get; set; }

        [JsonProperty("BTC")]
        public string Btc { get; set; }

        [JsonProperty("ETH")]
        public string Eth { get; set; }

        [JsonProperty("LTC")]
        public string Ltc { get; set; }

        [JsonProperty("NGT")]
        public string Ngt { get; set; }

        [JsonProperty("USDT")]
        public string Usdt { get; set; }

        [JsonProperty("BTC_USD")]
        public string BtcUsd { get; set; }

        [JsonProperty("ETH_USD")]
        public string EthUsd { get; set; }

        [JsonProperty("LTC_USD")]
        public string LtcUsd { get; set; }

        [JsonProperty("NGT_USD")]
        public string NgtUsd { get; set; }

        [JsonProperty("USDT_USD")]
        public string UsdtUsd { get; set; }

        [JsonProperty("BTC_24hr")]
        public string Btc24Hr { get; set; }

        [JsonProperty("ETH_24hr")]
        public string Eth24Hr { get; set; }

        [JsonProperty("LTC_24hr")]
        public string Ltc24Hr { get; set; }

        [JsonProperty("NGT_24hr")]
        public string Ngt24Hr { get; set; }

        [JsonProperty("USDT_24hr")]
        public string Usdt24Hr { get; set; }
    }
}
