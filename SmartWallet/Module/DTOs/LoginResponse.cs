﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class LoginResponse : ResponseBase
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("username")]
        public string Name { get; set; }


       
       
    }
}
