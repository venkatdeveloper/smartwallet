﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class CommunityListResponse : ResponseBase
    {
        [JsonProperty("items")]
        public List<CommunityMembers> UsersList { get; set; }
    }

    public class CommunityMembers
    {
        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("Personal")]
        public string PersonalStaking { get; set; }

        [JsonProperty("Community")]
        public string CommunityStaking { get; set; }

    }
}
