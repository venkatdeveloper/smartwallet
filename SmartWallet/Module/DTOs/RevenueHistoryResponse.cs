﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class RevenueHistoryResponse:ResponseBase
    {


        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("staking_coins")]
        public string StakingCoins { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }


    }
}
//date, staking_coins, amount