﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class RootResponse:ResponseBase
    {

        [JsonProperty("total_assets_balance")]
        public string TotalAssetsBalance { get; set; }


        [JsonProperty("coins")]
        public List<CoinDetails> CoinDetails { get; set; }

       // total_assets_balance, coins{ coin_name, coin_logo, coin_symbol, wallet_balance, market_rate_percentage, token_rate, wallet_address, transaction_fees
    }
    public class CoinDetails
    {
        [JsonProperty("coin_name")]
        public string CoinName { get; set; }

        [JsonProperty("coin_logo")]
        public string CoinLogo { get; set; }


        [JsonProperty("coin_symbol")]
        public string CoinSymbol { get; set; }

        [JsonProperty("wallet_balance")]
        public string WalletBalance { get; set; }

        [JsonProperty("market_rate_percentage")]
        public string MarketRatePercentage { get; set; }

        [JsonProperty("token_rate")]
        public string TokenRate { get; set; }


        [JsonProperty("wallet_address")]
        public string WalletAddress { get; set; }

        [JsonProperty("transaction_fees")]
        public string TransactionFees { get; set; }


        [JsonIgnore]
        public bool IsSelected { get; set; }
    }
}

