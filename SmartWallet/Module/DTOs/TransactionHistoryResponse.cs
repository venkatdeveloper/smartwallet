﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class TransactionHistoryResponse : ResponseBase
    {
        [JsonProperty("data")]
        public List<TransactionHistoryItems> TransactionHistory;
    }
    public class TransactionHistoryItems
    {
        [JsonProperty("trade_type")]
        public string TradeType { get; set; }

        [JsonProperty("trade_amount")]
        public string TradeAmount { get; set; }


        [JsonProperty("trade_status")]
        public string TradeStatus { get; set; }

        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonIgnore]
        public string Logo { get; set; }

    }
}
//status_code,staus, message, trade_type(deposit or withdraw), trade_amount, trade_status, date