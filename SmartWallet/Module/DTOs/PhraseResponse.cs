﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class PhraseResponse : ResponseBase
    {
        [JsonProperty("mnemonic")]
        public string MneMonic { get; set; }
    }
}
