﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SmartWallet.Module.DTOs
{
    public class CoinResponse : ResponseBase
    {
        [JsonProperty("coiname")]
        public string CoinName { get; set; }

        [JsonProperty("coinusdalue")]
        public string USDValue { get; set; }

        [JsonProperty("availablebalance")]
        public string AvailableBalance { get; set; }

        [JsonProperty("symbol")]
        public string CoinSymbol { get; set; }
    }
}
