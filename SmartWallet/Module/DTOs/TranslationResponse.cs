﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class TranslationResponse : ResponseBase
    {
        [JsonProperty("responseData")]
        public ResponseData ResponseData { get; set; }

        [JsonProperty("quotaFinished")]
        public bool QuotaFinished { get; set; }

        [JsonProperty("mtLangSupported")]
        public object MtLangSupported { get; set; }

        [JsonProperty("responseDetails")]
        public string ResponseDetails { get; set; }

        [JsonProperty("responseStatus")]
        public long ResponseStatus { get; set; }

        [JsonProperty("responderId")]
        public long ResponderId { get; set; }

        [JsonProperty("exception_code")]
        public object ExceptionCode { get; set; }

        [JsonProperty("matches")]
        public List<Match> Matches { get; set; }
    }

    public class Match
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("segment")]
        public string Segment { get; set; }

        [JsonProperty("translation")]
        public string Translation { get; set; }

        [JsonProperty("quality")]
        public long Quality { get; set; }

        [JsonProperty("reference")]
        public string Reference { get; set; }

        [JsonProperty("usage-count")]
        public long UsageCount { get; set; }

        [JsonProperty("subject")]
        public Subject Subject { get; set; }

        [JsonProperty("created-by")]
        public string CreatedBy { get; set; }

        [JsonProperty("last-updated-by")]
        public string LastUpdatedBy { get; set; }

        [JsonProperty("create-date")]
        public DateTimeOffset CreateDate { get; set; }

        [JsonProperty("last-update-date")]
        public DateTimeOffset LastUpdateDate { get; set; }

        [JsonProperty("match")]
        public double MatchMatch { get; set; }

        [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
        public string Model { get; set; }
    }

    public class ResponseData
    {
        [JsonProperty("translatedText")]
        public string TranslatedText { get; set; }

        [JsonProperty("match")]
        public double Match { get; set; }
    }

    public partial struct Subject
    {
        public bool? Bool;
        public string String;

        public static implicit operator Subject(bool Bool) => new Subject { Bool = Bool };
        public static implicit operator Subject(string String) => new Subject { String = String };
    }
}
