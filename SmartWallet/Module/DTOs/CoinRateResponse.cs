﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class CoinRateResponse : ResponseBase
    {
        [JsonProperty("usd_rate")]        
        public string UsdRate { get; set; }

        [JsonProperty("coincode")]
        public string Coincode { get; set; }        
    }
}
