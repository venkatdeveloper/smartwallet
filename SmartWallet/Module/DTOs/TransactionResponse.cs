﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class TransactionResponse : ResponseBase
    {
        [JsonProperty("responseData")]
        public TransactionData TransactionData { get; set; }
    }

    public class TransactionData
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("creditordebit")]
        public string CreditOrDebit { get; set; }

        [JsonProperty("toAddress")]
        public string ToAddress { get; set; }

        [JsonProperty("wallet")]
        public string Wallet { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }
    }
}
