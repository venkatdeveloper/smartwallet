﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class StakingDetailsResponse : ResponseBase
    {
        [JsonProperty("fullname")]        
        public string Fullname { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("PersonalStatcking")]
        public string PersonalStatcking { get; set; }

        [JsonProperty("TeamStatcking")]
        public string TeamStatcking { get; set; }
    }





    public class Users
    {
        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("personal_staking")]
        public string PersonalStaking { get; set; }

        [JsonProperty("community_staking")]
        public string CommunityStaking { get; set; }
    }


}



//personal_staking, total_community_staking, user_level, direcct_link_count, community_link_count, users: { username, level, personal_staking, community_staking}