﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class StakingHistoryResponse:ResponseBase
    {
        [JsonProperty("items")]
        public List<BuyHistoryItems> Items { get; set; }
    }

    public class BuyHistoryItems
    {
        [JsonProperty("BillNo")]        
        public string BillNo { get; set; }

        [JsonProperty("Date")]
        public string Date { get; set; }

        [JsonProperty("Amount")]
        public string Amount { get; set; }

        [JsonProperty("Wallet")]
        public string Wallet { get; set; }

        [JsonProperty("Wallet_Ammount")]
        public string WalletAmmount { get; set; }

        [JsonProperty("Runstatus")]
        public string Runstatus { get; set; }
    }
}
