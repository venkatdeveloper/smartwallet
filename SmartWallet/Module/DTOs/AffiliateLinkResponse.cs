﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class AffiliateLinkResponse : ResponseBase
    {
        [JsonProperty("qstring")]
        public string Qstring { get; set; }
    }
}
