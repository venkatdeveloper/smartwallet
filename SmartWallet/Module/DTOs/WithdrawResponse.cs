﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class WithdrawResponse : ResponseBase
    {
        [JsonProperty("wallet_balance")]
        public string WalletBalance { get; set; }
    }
}
