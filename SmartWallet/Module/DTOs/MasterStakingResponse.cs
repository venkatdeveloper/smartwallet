﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class MasterStakingResponse:ResponseBase
    {

        [JsonProperty("memberid")]
        public string Memberid { get; set; }

        [JsonProperty("coincode")]
        public string Coincode { get; set; }

        [JsonProperty("amount")]        
        public string Amount { get; set; }

    }
}
