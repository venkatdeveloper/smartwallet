﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class ProfileResponse : ResponseBase
    {
       
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("fullname")]
        public string Fullname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("countrycode")]        
        public string Countrycode { get; set; }

        [JsonProperty("joiningdate")]
        public string Joiningdate { get; set; }

        [JsonProperty("pushnotificationstatus")]
        public string Pushnotificationstatus { get; set; }
    }


}
//name, mobile_number, referral_code,kyc_status