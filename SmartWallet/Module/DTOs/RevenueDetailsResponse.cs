﻿using System;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class RevenueDetailsResponse : ResponseBase
    {

        [JsonProperty("TotalPersonalRevenue")]
        public string TotalPersonalRevenue { get; set; }

        [JsonProperty("TotalCommunityRevenue")]
        public string TotalCommunityRevenue { get; set; }

        [JsonProperty("TotalSwapedNG")]
        public string TotalSwapedNg { get; set; }

        [JsonProperty("AvailableRevenue")]
        public string AvailableRevenue { get; set; }

    }
}
//personal_staking_revenue, generation_revenue, community_revenue, loyalty_revenue, total_interstellar_revenue, total_staking_rate and total_revenue.