﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SmartWallet.Module.DTOs
{
    public class PersonalStakingResponse : ResponseBase
    {
        [JsonProperty("TotalPersonalRevenue")]
        public string TotalPersonalRevenue { get; set; }

        [JsonProperty("RevenueDetails")]
        public List<RevenueDetail> RevenueDetails { get; set; }
    }

    public class RevenueDetail
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("coincode")]
        public string CoinCode { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }
    }
}
