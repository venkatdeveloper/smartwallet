﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmartWallet.Module.Deposit.View
{
    public partial class DepositPage : ContentPage
    {
        public DepositPage()
        {
            InitializeComponent();

            qrCodeImageView.BarcodeOptions.Width = 300;
            qrCodeImageView.BarcodeOptions.Height = 300;
        }
    }
}
