﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
using SmartWallet.Module.Transactions.Model;
using SmartWallet.Services.Transaction;
using SmartWallet.Utils;
using Xamarin.Essentials;

namespace SmartWallet.Module.Deposit.ViewModel
{
    public class DepositViewModel : ViewModelBase
    {
        readonly ITransactionService transactionService;

        string qrCode = "3QqrC9YPXgz17L5irwZeGjgbhsXU4sarTO";
        public string QrCode
        {
            get => qrCode;
            set
            {
                qrCode = value;
                OnPropertyChanged();
            }
        }



        string logo;
        public string Logo
        {
            get => logo;
            set
            {
                logo = value;
                OnPropertyChanged();
            }
        }

        string receivedCoin;
        public string ReceivedCoin
        {
            get => receivedCoin;
            set
            {
                receivedCoin = value;
                OnPropertyChanged();
            }
        }

        public ICommand Copycmd => new AsyncCommand(OnCopy);
        public ICommand Sharefriendscmd => new AsyncCommand(Sharefriends);

        public DepositViewModel(ITransactionService transactionService)
        {
            this.transactionService = transactionService;
        }

        public override Task InitializeAsync(object navigationData) => Initialize(navigationData);
        async Task Initialize(object data)
        {
            if (data != null)
            {
                if (data is TransactionCoinsDetails coins)
                {
                    Logo = coins.CoinLogo;
                    ReceivedCoin = coins.CoinSymbol;
                    if (!NetworkUtils.IsConnected)
                    {
                        ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                        return;
                    }
                    IsBusy = true;
                    var response = await transactionService.DepositAsync(ReceivedCoin);
                    IsBusy = false;
                    if (!response.IsSuccess())
                    {
                        ToastService.ShowToast(response.Message);
                        QrCode = "Empty";
                        return;
                    }
                    QrCode = response.Address;
                    if (QrCode == string.Empty || QrCode == null)
                        QrCode = "Empty";
                }
            }

        }


        async Task OnCopy()
        {
            await Clipboard.SetTextAsync(QrCode);
            ToastService.ShowToast("Copied");
        }

        async Task Sharefriends()
        {
            await Share.RequestAsync(new ShareTextRequest
            {
                Text = QrCode,
                Title = "Share"
            });
        }
    }
}
