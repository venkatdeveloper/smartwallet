﻿using System;
using System.Collections.Generic;
using SmartWallet.Module.Common;
using SmartWallet.Module;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Services;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;
using SmartWallet.Extension;
using System.Windows.Input;

namespace SmartWallet.Module.Country.ViewModel
{
    public class CountryViewModel : ViewModelBase
    {
        readonly IAuthenticateService authenticateService;

        List<DTOs.Country> countryList;
        public List<DTOs.Country> CountryList
        {
            get => countryList;
            set
            {
                countryList = value;
                OnPropertyChanged();
            }
        }
        DTOs.Country selectedCountry;
        public DTOs.Country SelectedCountry
        {
            get => selectedCountry;
            set
            {
                selectedCountry = value;
                OnPropertyChanged();


            }
        }
        public ICommand SubmitCmd => new AsyncCommand(OnSubmit);

        public CountryViewModel(IAuthenticateService authenticateService)
        {
            this.authenticateService = authenticateService;
        }

        public override Task InitializeAsync(object navigationData) => GetCountryList();

        async Task GetCountryList()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            var response = await authenticateService.GetCountryListAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            CountryList = response.CountryList;
        }

        async Task OnClose() => await PopupNavigation.Instance.PopAllAsync();

        async Task OnSubmit()
        {
            if (SelectedCountry == null)
            {
                ToastService.ShowToast("Please Select One Country");
                return;
            }
            var previousViewModel = NavigationService.PreviousPageViewModel as IPopupCallback;
            await OnClose();
            await previousViewModel?.OnSubmit(SelectedCountry.PhoneCode);

        }
    }
}
