﻿using System.Threading.Tasks;

namespace SmartWallet.Module.Country.ViewModel
{
    public interface IPopupCallback
    {
        Task OnSubmit(object data);
    }
}