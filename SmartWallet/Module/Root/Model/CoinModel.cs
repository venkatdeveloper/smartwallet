﻿using System;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.DTOs;

namespace SmartWallet.Module.Root.Model
{
    public class CoinModel
    {
       
        public string CoinName { get; set; }

        
        public string CoinLogo { get; set; }


       
        public string CoinSymbol { get; set; }

       
        public string WalletBalance { get; set; }

       
        public string MarketRatePercentage { get; set; }

        public string Status { get; set; }


        public string StatusColor { get; set; }
        //public string TokenRate { get; set; }



        //public string WalletAddress { get; set; }


        public string TransctionFees { get; set; }
        public CoinModel(Coins coinDetails,string logo)
        {
            CoinName = coinDetails.FullName;
            CoinSymbol = coinDetails.Name;
            WalletBalance = coinDetails.AvailableVolume;
            MarketRatePercentage = coinDetails.MarketPercentage;

            CoinLogo = logo;
        }
    }
}
