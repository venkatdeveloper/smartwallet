﻿using System;
using System.Collections.Generic;
using SmartWallet.Module.Assets.View;
using SmartWallet.Module.Assets.ViewModel;
using SmartWallet.Module.Dashboard.View;
using SmartWallet.Module.Dashboard.ViewModel;
using SmartWallet.Module.Market.View;
using SmartWallet.Module.Market.ViewModel;
using SmartWallet.Module.Profile.View;
using SmartWallet.Module.Profile.ViewModel;
using SmartWallet.Module.Transactions.View;
using Xamarin.Forms;

namespace SmartWallet.Module.Root.View
{

        public partial class RootPage : ContentPage
        {
            public RootPage()
            {
                InitializeComponent();

                //BarTextColor = Color.Black;
                //SelectedTabColor = Color.Black;
                //UnselectedTabColor = Color.Silver;

                //Children.Add(new TransactionPage { Title = "Assets", IconImageSource = "assets" });
                //Children.Add(new MarketPage { Title = "Market", IconImageSource = "market", BindingContext = new MarketViewModel() });
                //Children.Add(new DashboardPage { Title = "S-Key", IconImageSource = "dashboard", BindingContext = new DashboardViewModel() });
                //Children.Add(new ProfilePage { Title = "Me", IconImageSource = "profile", BindingContext = new ProfileViewModel() });
            }

            //protected override void OnAppearing()
            //{
            // MessagingCenter.Instance.Subscribe<RootViewModel>(this, Constants.HIDE_MASTER_KEY, (obj) => { IsPresented = false; });
            // base.OnAppearing();
            //}

            //protected override void OnDisappearing()
            //{
            // MessagingCenter.Instance.Unsubscribe<RootViewModel>(this, Constants.HIDE_MASTER_KEY);
            // base.OnDisappearing();
            //}
        }

}
