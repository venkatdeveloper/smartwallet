﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
using SmartWallet.Module.Dashboard.ViewModel;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Module.Profile.ViewModel;
using SmartWallet.Module.Root.Model;
using SmartWallet.Module.Transactions.ViewModel;
using SmartWallet.Services.Account;
using SmartWallet.Services.Dialog;
using SmartWallet.Utils;

namespace SmartWallet.Module.Root.ViewModel
{
    public class RootViewModel : ViewModelBase
    {
        readonly IAccountService accountService;
        readonly IDialogService dialogService;

        ObservableCollection<Coins> coinsList;
        public ObservableCollection<Coins> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }

        ObservableCollection<string> CoinTempList = new ObservableCollection<string>
        {
            "Bitcoin","Etherium","ltc","logo","usdt"
        };


        string availableBalance;
        public string AvailableBalance
        {
            get => availableBalance;
            set
            {
                availableBalance = value;
                OnPropertyChanged();
            }

        }


        string username = AppSettings.UserName;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }

        }

        string logo;
        public string Logo
        {
            get => logo;
            set
            {
                logo = value;
                OnPropertyChanged();
            }

        }

        public ICommand GotoTransactionCmd => new AsyncCommand(OnTransaction);
        async Task OnTransaction() => await NavigationService.NavigateToAsync<TransactionViewModel>();


        public ICommand GotoDashBoardCmd => new AsyncCommand(OnDashboard);
        async Task OnDashboard() => await NavigationService.NavigateToAsync<DashboardViewModel>();


        public ICommand GotoProfileCmd => new AsyncCommand(OnProfile);
        async Task OnProfile() => await NavigationService.NavigateToAsync<ProfileViewModel>();


        public RootViewModel(IAccountService accountService, IDialogService dialogService)
        {
            this.accountService = accountService;
            this.dialogService = dialogService;


        }
        public override Task InitializeAsync(object navigationData) => InitialSetup();
        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await accountService.DashboardDetailsAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                
                if (response.Message == "Token Expired")
                {
                    await dialogService.ShowAlertAsync("Next Gen", Constants.TOKEN_EXPIRED, Constants.OK);
                    AppSettings.Clear();
                    await NavigationService.NavigateToAsync<LoginViewModel>();
                }
                return;
            }
            AvailableBalance = response.Totalasset;
            Username = response.Fullname;
            CoinsList = new ObservableCollection<Coins>
           {
               new Coins
               {
                   Name="BTC",Logo="Bitcoin",AvailableVolume = response.Btc,MarketPercentage = response.Btc24Hr
               },
               new Coins
               {
                   Name="ETH",Logo="Etherium",AvailableVolume = response.Eth,MarketPercentage = response.Eth24Hr
               },
               new Coins
               {
                   Name="LTC",Logo="ltc",AvailableVolume = response.Ltc,MarketPercentage = response.Ltc24Hr
               },
                new Coins
               {
                   Name="NGT",Logo="logo",AvailableVolume = response.Ngt,MarketPercentage = response.Ngt24Hr
               },

                 new Coins
               {
                   Name="USDT",Logo="usdt",AvailableVolume = response.Usdt,MarketPercentage = response.Usdt24Hr
               },

           };
        }

    }
}
