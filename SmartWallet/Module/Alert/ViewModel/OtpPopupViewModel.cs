﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Rg.Plugins.Popup.Services;
using SmartWallet.Module.Common;
using SmartWallet.Module.Country.ViewModel;
using SmartWallet.Utils;

namespace SmartWallet.Module.Alert.ViewModel
{
    public class OtpPopupViewModel : ViewModelBase
    {
        string otp;
        public string Otp
        {
            get => otp;
            set
            {
                otp = value;
                OnPropertyChanged();


            }
        }
        public ICommand SubmitCmd => new AsyncCommand(OnSubmit);
        public ICommand CloseCmd => new AsyncCommand(OnClose);
        public OtpPopupViewModel()
        {
        }
        async Task OnClose() => await PopupNavigation.Instance.PopAllAsync();

        async Task OnSubmit()
        {
            if (Otp == null)
            {
                ToastService.ShowToast("Please Enter OTP");
                return;
            }
            var previousViewModel = NavigationService.PreviousPageViewModel as IPopupCallback;
            await OnClose();
            await previousViewModel?.OnSubmit(Otp);
        }
    }
}
