﻿using System;
using Newtonsoft.Json;
using SmartWallet.Module.Common;

namespace SmartWallet.Module.Register.Model
{
    public class RegisterRequestModel
    {
        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("countrycode")]
        public string Countrycode { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("smscode")]
        public string Smscode { get; set; }

        [JsonProperty("fullname")]
        public string Fullname { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("govtid")]
        public string Govtid { get; set; }

        [JsonProperty("referral")]
        public string Referral { get; set; }

        [JsonProperty("loginpassword")]
        public string Loginpassword { get; set; }

        [JsonProperty("tradingpassword")]
        public string Tradingpassword { get; set; }
        
        public RegisterRequestModel()
        {
        }
    }
}
