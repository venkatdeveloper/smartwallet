﻿using System;
using SmartWallet.Module.Common;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;
using Xamarin.Forms;
using SmartWallet.Services.Navigation;
using SmartWallet.Module.Login.ViewModel;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Module.Assets.ViewModel;
using SmartWallet.Extension;
using SmartWallet.Module.Register.Model;
using SmartWallet.Module.Phrase.ViewModel;
using SmartWallet.Module.Root.ViewModel;
using System.Collections.Generic;
using SmartWallet.Module.DTOs;
using SmartWallet.Module.Country.ViewModel;

namespace SmartWallet.Module.Register.ViewModel
{
    public class RegisterViewModal : ViewModelBase, IPopupCallback
    {
        readonly IAuthenticateService authenticateService;


        string name = string.Empty;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        string mobileno = string.Empty;
        public string Mobileno
        {
            get => mobileno;
            set
            {
                mobileno = value;
                OnPropertyChanged();

            }
        }

        string verificationcode;
        public string Verificationcode
        {
            get => verificationcode;
            set
            {
                verificationcode = value;
                OnPropertyChanged();


            }
        }

        bool checkBox;
        public bool CheckBox
        {
            get => checkBox;
            set
            {
                checkBox = value;
                OnPropertyChanged();


            }
        }

        string nameinfo = string.Empty;
        public string Nameinfo
        {
            get => nameinfo;
            set
            {
                nameinfo = value;
                OnPropertyChanged();


            }
        }

        string email = string.Empty;
        public string Email
        {
            get => email;
            set
            {
                email = value;
                OnPropertyChanged();


            }
        }
        string countryCode = "91";
        public string CountryCode
        {
            get => countryCode;
            set
            {
                countryCode = value;
                OnPropertyChanged();
            }
        }

        string govtId;
        public string GovtId
        {
            get => govtId;
            set
            {
                govtId = value;
                OnPropertyChanged();


            }
        }

        string referCode;
        public string ReferCode
        {
            get => referCode;
            set
            {
                referCode = value;
                OnPropertyChanged();

            }
        }

        bool canEnableNextButton;
        public bool CanEnableNextButton
        {
            get => canEnableNextButton;
            set
            {
                canEnableNextButton = value;
                OnPropertyChanged();
            }
        }

        string enterpassword;
        public string Enterpassword
        {
            get => enterpassword;
            set
            {
                enterpassword = value;
                OnPropertyChanged();
            }
        }

        string reenterpassword;
        public string ReEnterpassword
        {
            get => reenterpassword;
            set
            {
                reenterpassword = value;
                OnPropertyChanged();
            }
        }

        string entertradingpassword;
        public string Entertradingpassword
        {
            get => entertradingpassword;
            set
            {
                entertradingpassword = value;
                OnPropertyChanged();
            }
        }

        string reentertradingpassword;
        public string ReEntertradingpassword
        {
            get => reentertradingpassword;
            set
            {
                reentertradingpassword = value;
                OnPropertyChanged();
            }
        }

        bool inititalStack;
        public bool InitialStack
        {
            get => inititalStack;
            set
            {
                inititalStack = value;
                OnPropertyChanged();
            }
        }

        bool informationStack;
        public bool InformationStack
        {
            get => informationStack;
            set
            {
                informationStack = value;
                OnPropertyChanged();
            }
        }

        bool passwordStack;
        public bool PasswordStack
        {
            get => passwordStack;
            set
            {
                passwordStack = value;
                OnPropertyChanged();
            }
        }

        bool tradepasswordStack;
        public bool TradePasswordStack
        {
            get => tradepasswordStack;
            set
            {
                tradepasswordStack = value;
                OnPropertyChanged();
            }
        }

        bool isPassword = true;
        public bool IsPassword
        {
            get => isPassword;
            set
            {
                isPassword = value;
                OnPropertyChanged();
            }
        }

        string passwordImage = "eye";
        public string PasswordImage
        {
            get => passwordImage;
            set
            {
                passwordImage = value;
                OnPropertyChanged();
            }
        }

        public ICommand Gotologincmd => new AsyncCommand(OnGotologin);
        public ICommand RegisterationStepsCmd => new AsyncCommand(OnRegister);
        public ICommand ChangePasswordTextCmd => new AsyncCommand(ChangePasswordText);
        public ICommand GetCountryCmd => new AsyncCommand(() => NavigationService.NavigatePopupAsync<CountryViewModel>());
        public RegisterViewModal(IAuthenticateService authenticateService)
        {
            this.authenticateService = authenticateService;
            InitialStack = true;
        }

        //password lock method
        async Task ChangePasswordText()
        {
            if (IsPassword)
            {
                IsPassword = false;
                PasswordImage = "eyecross";
            }
            else
            {
                IsPassword = true;
                PasswordImage = "eye";
            }
            await Task.CompletedTask;
        }

        async Task OnGotologin() => await NavigationService.NavigateToAsync<LoginViewModel>();

        async Task OnRegister(object stepNo)
        {
            switch (stepNo.ToString())
            {
                case "1":
                    {
                        if (IsValidSmsRequest())
                        {
                            var res = await OnDone(1);
                        }
                        break;
                    }
                case "2":
                    {
                        if (IsValidSignup())
                        {
                            var res = await OnDone(2);
                            if (res)
                            {
                                InitialStack = false;
                                InformationStack = true;
                            }
                        }
                        break;
                    }
                case "3":
                    {
                        if (IsValidPersonalInfo())
                        {
                            var res = await OnDone(3);
                            if (res)
                            {
                                PasswordStack = true;
                                InformationStack = false;
                            }
                        }
                        break;
                    }
                case "4":
                    {
                        if (IsValidPassword())
                        {
                            var res = await OnDone(4);
                            if (res)
                            {
                                PasswordStack = false;
                                TradePasswordStack = true;
                            }
                        }
                        break;
                    }
                case "5":
                    {
                        if (IsValidTradePassword())
                        {
                            var res = await OnDone(5);
                            if (res)
                            {
                                await NavigationService.NavigateToAsync<PhraseViewModel>();
                            }
                        }
                        break;
                    }
            }
        }

        async Task<bool> OnDone(int step)
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return false;
            }
            IsBusy = true;
            var requestModel = new RegisterRequestModel
            {
                Username = Name,
                Countrycode = CountryCode,
                Mobile = Mobileno,
                Smscode = Verificationcode,
                Fullname = Nameinfo,
                Email = Email,
                Govtid = GovtId,
                Referral = ReferCode,
                Loginpassword = Enterpassword,
                Tradingpassword = Entertradingpassword,
            };
            var response = await authenticateService.RegisterAsync(requestModel, step);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return false;
            }
            if (step == 1)
            {
                ToastService.ShowToast(response.Message);
            }
            return true;
        }

        bool IsValidSmsRequest()
        {
            var isValid = false;
            if (!Name.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else if (!Mobileno.IsValid())
                ToastService.ShowToast(Constants.EMPTY_MOBILENUMBER);
            else isValid = true;
            return isValid;
        }


        bool IsValidSignup()
        {
            var isValid = false;
            if (!Name.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else if (!Mobileno.IsValid())
                ToastService.ShowToast(Constants.EMPTY_MOBILENUMBER);
            else if (!Verificationcode.IsValid())
                ToastService.ShowToast(Constants.EMPTY_VERIFICATIONCODE);
            else if (!CheckBox)
                ToastService.ShowToast(Constants.EMPTY_CHECKBOX);
            else isValid = true;
            return isValid;
        }

        bool IsValidPersonalInfo()
        {
            var isValid = false;
            if (!Nameinfo.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else if (!Email.IsValidMail())
                ToastService.ShowToast(Constants.EMPTY_EMAIL);
            else if (!GovtId.IsValid())
                ToastService.ShowToast(Constants.EMPTY_GOVTID);
            else if (!ReferCode.IsValid())
                ToastService.ShowToast(Constants.EMPTY_REFERCODE);
            else isValid = true;
            return isValid;
        }
        bool IsValidPassword()
        {
            var isValid = false;
            if (!Enterpassword.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (!ReEnterpassword.IsValid())
                ToastService.ShowToast(Constants.REENTER_PASSWORD);
            else if (Enterpassword != ReEnterpassword)
                ToastService.ShowToast(Constants.PASSWORD_MISMATCH);
            else isValid = true;
            return isValid;
        }

        bool IsValidTradePassword()
        {
            var isValid = false;
            if (!Entertradingpassword.IsValid())
                ToastService.ShowToast(Constants.EMPTY_PASSWORD);
            else if (!ReEntertradingpassword.IsValid())
                ToastService.ShowToast(Constants.REENTER_PASSWORD);
            else if (Entertradingpassword != ReEntertradingpassword)
                ToastService.ShowToast(Constants.PASSWORD_MISMATCH);
            else isValid = true;
            return isValid;
        }

        public async Task OnSubmit(object data)
        {
            CountryCode = data.ToString();
            await Task.CompletedTask;
        }
    }
}
