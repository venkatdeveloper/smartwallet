﻿using System;
using System.Collections.Generic;
using SmartWallet.Module.Register.ViewModel;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace SmartWallet.Module.Register.View
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();            
        }

        void OnScanQRCode(object sender, EventArgs e)
        {

            var scannerPage = new ZXingScannerPage();
            Navigation.PushAsync(scannerPage);
            scannerPage.OnScanResult += (result) =>
            {
                scannerPage.IsScanning = false;
                ZXing.BarcodeFormat barcodeFormat = result.BarcodeFormat;
                string type = barcodeFormat.ToString();
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PopAsync();
                    (BindingContext as RegisterViewModal).ReferCode = result.Text;


                });

            };
        }
    }
}
