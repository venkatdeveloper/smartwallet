﻿using System;
namespace SmartWallet.Module.Phrase.Model
{
    public class PhraseModel
    {
        public string PhraseText { get;private set; }
        public PhraseModel(string text)
        {
            PhraseText = text;
        }
    }
}
