﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.Login.ViewModel;
using SmartWallet.Module.Phrase.Model;
using SmartWallet.Services.Phrase;
using SmartWallet.Utils;
using Xamarin.Essentials;

namespace SmartWallet.Module.Phrase.ViewModel
{
    public class PhraseViewModel : ViewModelBase
    {
        readonly IPhraseServices phraseServices;
        ObservableCollection<PhraseModel> selectedPhraseList = new ObservableCollection<PhraseModel>();
        public ObservableCollection<PhraseModel> SelectedPhraseList
        {
            get => selectedPhraseList;
            set
            {
                selectedPhraseList = value;
                OnPropertyChanged();
            }
        }
        PhraseModel selectedPhraseForRemove;
        public PhraseModel SelectedPhraseForRemove
        {
            get => selectedPhraseForRemove;
            set
            {
                selectedPhraseForRemove = value;
                OnPropertyChanged();
                //   AddAndRemovePhraseList();
            }
        }
        ObservableCollection<PhraseModel> backUpPhraseList;
        public ObservableCollection<PhraseModel> BackUpPhraseList
        {
            get => backUpPhraseList;
            set
            {
                backUpPhraseList = value;
                OnPropertyChanged();
            }
        }
        ObservableCollection<PhraseModel> phraseList = new ObservableCollection<PhraseModel>();
        public ObservableCollection<PhraseModel> PhraseList
        {
            get => phraseList;
            set
            {
                phraseList = value;
                OnPropertyChanged();
            }
        }
        PhraseModel selectedPhrase;
        public PhraseModel SelectedPhrase
        {
            get => selectedPhrase;
            set
            {
                selectedPhrase = value;
                OnPropertyChanged();
                // AddToSelectedPhraseList();
            }
        }
        bool phraseStartStack = true;
        public bool PhraseStartStack
        {
            get => phraseStartStack;
            set
            {
                phraseStartStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseSelectStack;
        public bool PhraseSelectStack
        {
            get => phraseSelectStack;
            set
            {
                phraseSelectStack = value;
                OnPropertyChanged();
            }
        }
        bool greetingsStack;
        public bool GreetingsStack
        {
            get => greetingsStack;
            set
            {
                greetingsStack = value;
                OnPropertyChanged();
            }
        }

        string phraseWords;
        public string PhraseWords
        {
            get => phraseWords;
            set
            {
                phraseWords = value;
                OnPropertyChanged();
            }
        }

        // public ICommand NextCmd => new AsyncCommand(OnNext);
        public ICommand CopyCmd => new AsyncCommand(OnCopy);
        public ICommand DownloadCmd => new AsyncCommand(OnDownload);
        public ICommand LoginCmd => new AsyncCommand(() => NavigationService.NavigateBackAsync());
        // public ICommand DoneCmd => new AsyncCommand(OnGreetings);

        public PhraseViewModel(IPhraseServices phraseServices)
        {
            this.phraseServices = phraseServices;
        }

        public override Task InitializeAsync(object navigationData) => GetPhraseAsync();

        async Task GetPhraseAsync()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await phraseServices.GetMneMonicPhraseAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                if (response.Message.Contains("exist"))
                {
                    await ShowPhraseAsync();
                }
                return;
            }
            PhraseWords = response.MneMonic;
            await OnCreatePhrase();
            //var items = response.MneMonic.Split(new char[0]);
            //foreach (var item in items)
            //{
            //    PhraseList.Add(new PhraseModel(item));
            //}
            //BackUpPhraseList = PhraseList;
        }

        async Task ShowPhraseAsync()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await phraseServices.ShowMneMonicPhraseAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            PhraseWords = response.MneMonic;
        }

        async Task OnCreatePhrase()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await phraseServices.CreateMnemonicPhrase(PhraseWords);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
        }

        //async Task OnNext()
        //{
        //    PhraseStartStack = false;
        //    PhraseSelectStack = true;
        //    await Task.CompletedTask;
        //}

        async Task OnCopy()
        {
            if (PhraseStartStack && PhraseSelectStack)
            {
                await Clipboard.SetTextAsync(PhraseWords);
            }
            else
            {
                await Clipboard.SetTextAsync(PhraseWords);
            }
            ToastService.ShowToast("Copied");
            await Task.CompletedTask;
        }
        async Task OnDownload()
        {
            ToastService.ShowToast("Downloaded");
            await Task.CompletedTask;
        }
        //async Task OnLogin()
        //{
        //    await NavigationService.NavigateToAsync<LoginViewModel>();
        //}
        //async Task OnGreetings()
        //{
        //    string phrWords = "";
        //    for (int i = 0; i < BackUpPhraseList.Count; i++)
        //    {
        //        if (BackUpPhraseList[i].PhraseText != SelectedPhraseList[i].PhraseText)
        //        {
        //            ToastService.ShowToast($"Incorrect phrase selection in position: {i + 1} ");
        //            return;
        //        }
        //        phrWords += $" {SelectedPhraseList[i].PhraseText}"; 
        //    }
        //    
        //    ToastService.ShowToast(response.Message);
        //    PhraseSelectStack = false;
        //    GreetingsStack = true;

        //}
        //void AddToSelectedPhraseList()
        //{
        //    SelectedPhraseList.Add(SelectedPhrase);
        //    PhraseList.Remove(SelectedPhrase);
        //}
        //void AddAndRemovePhraseList()
        //{
        //    PhraseList.Add(SelectedPhraseForRemove);
        //    SelectedPhraseList.Remove(SelectedPhraseForRemove);
        //}
    }
}
