﻿using System;
using SmartWallet.Module.Common;

namespace SmartWallet.Module.Transactions.Model
{
    public class TransactionCoinsDetails : ViewModelBase
    {
        public string CoinName { get; set; }
        public string CoinLogo { get; set; }
        public string CoinSymbol { get; set; }
        public string WalletBalance { get; set; }
        public string MarketRatePercentage { get; set; }
        public string TokenRate { get; set; }
        public string WalletAddress { get; set; }
        public string TransactionFees { get; set; }

        bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
                OnPropertyChanged(nameof(TextColor));
            }
        }
        public TransactionCoinsDetails()
        {
        }
        public string TextColor => IsSelected ? "White" : "Black";
    }
}
