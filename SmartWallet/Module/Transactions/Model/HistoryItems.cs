﻿using System;
using Xamarin.Forms;

namespace SmartWallet.Module.Transactions.Model
{
    public class HistoryItems
    {

        
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string TransferStatus { get; set; }
        public string Logo => TransferStatus == "Send" ? "leftdown" : "rightup";
        public Color StatusColor => Status == "PENDING" ? Color.Yellow : Color.Green;

        public HistoryItems()
        {
        }
    }
}
