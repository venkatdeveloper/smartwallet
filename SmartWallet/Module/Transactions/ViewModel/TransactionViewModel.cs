﻿using System;
using SmartWallet.Module.Common;
using SmartWallet.Services.Authenticate;
using SmartWallet.Module.Deposit.ViewModel;
using System.Windows.Input;
using SmartWallet.Utils;
using System.Threading.Tasks;
using SmartWallet.Module.Withdraw.ViewModel;
using SmartWallet.Module.Assets.Model;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using System.Collections.Generic;
using SmartWallet.Module.Swap.ViewModel;
using SmartWallet.Module.Transactions.Model;
using SmartWallet.Services.Transaction;
using SmartWallet.Extension;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Account;

namespace SmartWallet.Module.Transactions.ViewModel
{
    public class TransactionViewModel : ViewModelBase
    {
        readonly ITransactionService transactionService;
        readonly IAccountService accountService;
        List<string> filter;
        public List<string> Filter
        {
            get => filter;
            set
            {
                filter = value;
                OnPropertyChanged();
            }
        }



        string selectedFilterValue;
        public string SelectedFilterValue
        {
            get => selectedFilterValue;
            set
            {
                selectedFilterValue = value;
                OnPropertyChanged();
            }
        }

        string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }
        bool canShowMenuList;
        public bool CanShowMenuList
        {
            get => canShowMenuList;
            set
            {
                canShowMenuList = value;
                OnPropertyChanged();
            }
        }
        List<TransactionCoinsDetails> coinList;
        public List<TransactionCoinsDetails> CoinList
        {
            get => coinList;
            set
            {
                coinList = value;
                OnPropertyChanged();
            }
        }
        TransactionCoinsDetails selectedCoin;
        public TransactionCoinsDetails SelectedCoin
        {
            get => selectedCoin;
            set
            {
                selectedCoin = value;
                OnPropertyChanged();
                CoinChanged();
                _ = OnHistory();
            }
        }

        string amount;
        public string Amount
        {
            get => amount;
            set
            {
                amount = value;
                OnPropertyChanged();
            }

        }
        string logo;
        public string Logo
        {
            get => logo;
            set
            {
                logo = value;
                OnPropertyChanged();
            }

        }


        string btc = "BTC";
        public string BTC
        {
            get => btc;
            set
            {
                btc = value;
                OnPropertyChanged();
            }

        }


        string btcvalue = "0.00";
        public string BTCvalue
        {
            get => btcvalue;
            set
            {
                btcvalue = value;
                OnPropertyChanged();
            }

        }



        string fullName;
        public string FullName
        {
            get => fullName;
            set
            {
                fullName = value;
                OnPropertyChanged();
            }

        }











        string selectedMenu;
        public string SelectedMenu
        {
            get => selectedMenu;
            set
            {
                selectedMenu = value;
                OnPropertyChanged();
            }
        }

        bool canShowEmpty;
        public bool CanShowEmpty
        {
            get => canShowEmpty;
            set
            {
                canShowEmpty = value;
                OnPropertyChanged();
            }
        }

        List<TransactionHistoryItems> historyList;
        public List<TransactionHistoryItems> HistoryList
        {
            get => historyList;
            set
            {
                historyList = value;
                OnPropertyChanged();
            }
        }
        public ICommand GotoDepositCmd => new AsyncCommand(OnDeposit);
        public ICommand GotoWithdrawcmd => new AsyncCommand(OnWithdraw);
        public ICommand GotoSwapcmd => new AsyncCommand(OnSwap);
        //public ICommand ShowMenuListCmd => new AsyncCommand(ShowMenuList);
        public ICommand LeftCmd => new AsyncCommand(OnDeposit);
        public ICommand RightCmd => new AsyncCommand(OnWithdraw);
        public ICommand ItemTappedCmd => new AsyncCommand(OnItemTapped);
        public TransactionViewModel(ITransactionService transactionService, IAccountService accountService)
        {
            this.transactionService = transactionService;
            this.accountService = accountService;
            // SetupMenuData();
        }

        public override Task InitializeAsync(object navigationData) => InitialSetup();
        async Task InitialSetup()
        {
            await SetupCoin();
            // await OnHistory();
            //await CoinSetup();
        }

        async Task OnItemTapped(object data)
        {
            foreach (var item in CoinList)
            {
                if (item.CoinSymbol == data.ToString())
                {
                    SelectedCoin = item;
                }
            }
            await Task.CompletedTask;
        }

        void SetupMenuData()
        {
            //SelectedCoin = CoinList[0];
            //HistoryList = new List<TransactionHistoryItems> {
            //        new TransactionHistoryItems {
            //           TradeAmount="12,393939",
            //           Date="12-07-2019",
            //           TradeStatus="PENDING",
            //           TradeType="Send",
            //           Logo = "Bitcoin"

            //        },
            //        new TransactionHistoryItems {
            //            TradeAmount="13,303030",
            //           Date="13-07-2019",
            //          TradeStatus="SUCCESS",
            //           TradeType="Recieve",
            //           Logo = "Etherium"

            //        },
            //         new TransactionHistoryItems {
            //            TradeAmount="14,303030",
            //           Date="14-07-2019",
            //            TradeStatus="PENDING",
            //           TradeType="Send",
            //           Logo = "ltc"
            //        },
            //        new TransactionHistoryItems {
            //            TradeAmount="15,303030",
            //           Date="15-07-2019",
            //          TradeStatus="SUCCESS",
            //           TradeType="Recieve",
            //           Logo = "ltc"
            //        },

            //             new TransactionHistoryItems {
            //             TradeAmount="16,303030",
            //           Date="16-07-2019",
            //            TradeStatus="PENDING",
            //           TradeType="Send",
            //           Logo = "usdt"
            //        },
            //    };



        }



        async Task OnDeposit() => await NavigationService.NavigateToAsync<DepositViewModel>(SelectedCoin);

        async Task OnWithdraw() => await NavigationService.NavigateToAsync<WithdrawViewModel>(SelectedCoin);

        async Task OnSwap()
        {
            if (SelectedCoin.CoinSymbol == "NGT")
                await NavigationService.NavigateToAsync<SwapViewModel>(SelectedCoin);
            else
                ToastService.ShowToast("Only NGT Can Allowed for Swapping");
        }

        async Task OnHistory()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await transactionService.GetTransactionHistoryAsync(SelectedCoin.CoinSymbol);
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            HistoryList = response.TransactionHistory;
            CanShowEmpty = HistoryList == null;
        }

        async Task SetupCoin()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await accountService.DashboardDetailsAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            CoinList = new List<TransactionCoinsDetails>
            {
                new TransactionCoinsDetails
                {
                    CoinSymbol="BTC",
                    CoinName = "BitCoin",
                    IsSelected = false,
                    CoinLogo="Bitcoin",
                    WalletBalance=response.Btc,
                    TokenRate=response.BtcUsd,
                },
                new TransactionCoinsDetails
                {
                    CoinSymbol="ETH",
                    CoinName = "Ethereum",
                    IsSelected = false,
                    CoinLogo="Etherium",
                    WalletBalance=response.Eth,
                    TokenRate=response.EthUsd,
                },
                new TransactionCoinsDetails
                {
                    CoinSymbol="USDT",
                    CoinName = "USDT",
                    IsSelected = false,
                    CoinLogo="usdt",
                    WalletBalance=response.Usdt,
                    TokenRate=response.UsdtUsd,
                },
                new TransactionCoinsDetails
                {
                    CoinSymbol="LTC",
                    CoinName = "LiteCoin",
                    IsSelected = false,
                    CoinLogo="ltc",
                    WalletBalance=response.Ltc,
                    TokenRate=response.LtcUsd,
                },
                new TransactionCoinsDetails
                {
                    CoinSymbol="NGT",
                    CoinName = "NG Token",
                    IsSelected = false,
                    CoinLogo="logo",
                    WalletBalance=response.Ngt,
                    TokenRate=response.NgtUsd,
                }
            };
            SelectedCoin = CoinList[0];
        }

        void CoinChanged()
        {
            foreach (var item in CoinList)
            {
                item.IsSelected = false;
            }
            SelectedCoin.IsSelected = true;
            Name = SelectedCoin.CoinSymbol;
            FullName = SelectedCoin.CoinName;
            Logo = SelectedCoin.CoinLogo;
            Amount = SelectedCoin.WalletBalance;
            BTCvalue = SelectedCoin.TokenRate;
            OnPropertyChanged(nameof(CoinList));
        }
    }
}

