﻿using System;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.Revenue.Model;

namespace SmartWallet.Module.GenerationRevenue.ViewModel
{
    public class GenerationRevenueViewModel:ViewModelBase
    {
        string name;
        public string Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged();
            }
        }

        string amount;
        public string Amount
        {
            get => amount;
            set
            {
                amount = value;
                OnPropertyChanged();
            }
        }


        string date = "2019-07-28";
        public string Date
        {
            get => date;
            set
            {
                date = value;
                OnPropertyChanged();
            }
        }

        string amountValue = "0.26";
        public string AmountValue
        {
            get => amountValue;
            set
            {
                amountValue = value;
                OnPropertyChanged();
            }
        }
        public GenerationRevenueViewModel()
        { 

        }




        



        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);
        async Task InitialSetup(object data)
        {
            if(data!=null)
            {
                if (data is RevenueNavigationData revenue)
                {
                    Name = revenue.Name;
                    Amount = revenue.Amount;
                }
                
            }await Task.CompletedTask;
        }

    }
}
