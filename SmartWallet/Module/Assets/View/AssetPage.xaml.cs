﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmartWallet.Module.Assets.View
{
    public partial class AssetPage : ContentPage
    {
        public AssetPage()
        {
            InitializeComponent();
        }

        void ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}
