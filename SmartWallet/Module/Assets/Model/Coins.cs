﻿using System;
using SmartWallet.Module.Common;
using Xamarin.Forms;

namespace SmartWallet.Module.Assets.Model
{
    public class Coins:ViewModelBase
    {      
        public string Logo { get; set; }
        string availableVolume;
        public string AvailableVolume
        {
            get => availableVolume;
            set
            {
                availableVolume = value;
                OnPropertyChanged();
            }
        }
        public string Rate { get; set; }
        string marketPercentage;
        public string MarketPercentage
        {
            get => marketPercentage;
            set
            {
                marketPercentage = value;
                OnPropertyChanged();
            }
        }
        public string Fee { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string QrAddress { get; set; }
        public string Status { get; set; }
        public Color StatusColor => Status == "-" ? Color.Red : Color.Green;

        bool isSelected;
        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                OnPropertyChanged(nameof(IsSelected));
                OnPropertyChanged(nameof(TextColor));
            }
        }

        public Color TextColor => IsSelected ? Color.White : Color.Black;

        
        public Coins()
        {
        }
    }
}
