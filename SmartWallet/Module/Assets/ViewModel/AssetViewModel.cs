﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Module.Common;
using SmartWallet.Module.Transactions.ViewModel;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;
using System.Collections.Generic;
using SmartWallet.Module.Assets.Model;
using System.Dynamic;

namespace SmartWallet.Module.Assets.ViewModel
{
   

    public class AssetViewModel:ViewModelBase
    {

        List<Coins> coinsList;
        public List<Coins> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }

        Coins selectedCoin;
        public Coins SelectedCoin
        {

            get => selectedCoin;
            set
            {

                selectedCoin = value;
                OnPropertyChanged();
                var ignore =  Test(value);
            }
        }


        public AssetViewModel()
        {
            CoinsList = new List<Coins>
           {
               new Coins
               {
                   Name="BTC",AvailableVolume="20200",Rate="39393"


               },
               new Coins
               {
                   Name="ETH",AvailableVolume="7474",Rate="8484"


               },
               new Coins
               {
                   Name="LTC",AvailableVolume="4646",Rate="3733"

               },

                new Coins
               {
                   Name="MOD",AvailableVolume="4946",Rate="3733"

               },

                 new Coins
               {
                   Name="USDT",AvailableVolume="4886",Rate="66733"

               },
              
           };
        }


       



     




        public ICommand GotoTransactionCmd => new AsyncCommand(OnTransaction);
      


        async Task OnTransaction() => await NavigationService.NavigateToAsync<TransactionViewModel>();


        async Task Test(Coins coins)
        {
            await NavigationService.NavigateToAsync<TransactionViewModel>(coins);
        }


    }
}
