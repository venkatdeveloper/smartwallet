﻿using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Module.Common;
using SmartWallet.Utils;
using Xamarin.Essentials;

namespace SmartWallet.Module.About.ViewModel
{
    public class AboutViewModel:ViewModelBase
    {


        string versionnumber = "1.0.0.6";
        public string VersionNumber
        {
            get => versionnumber;
            set
            {
                versionnumber = value;
                OnPropertyChanged();
            }
        }


        public ICommand Officialwebsitecmd => new AsyncCommand(OnOfficialWebsite);


       


         async Task OnOfficialWebsite()
        {
            await Browser.OpenAsync("https://www.smartwallet.com");
        }


        public AboutViewModel()


        {


        }
    }
}
