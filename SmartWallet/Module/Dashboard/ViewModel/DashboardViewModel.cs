﻿using System;
using SmartWallet.Module.Common;
using SmartWallet.Module.MasterStaking.ViewModel;
using SmartWallet.Module.Community.ViewModel;
using SmartWallet.Module.Revenue.ViewModel;
using System.Windows.Input;
using SmartWallet.Utils;
using System.Threading.Tasks;
using SmartWallet.Module.StakingRecord.ViewModel;
using SmartWallet.Services.Transaction;
using SmartWallet.Extension;
using SmartWallet.Services.Market;

namespace SmartWallet.Module.Dashboard.ViewModel
{
    public class DashboardViewModel : ViewModelBase
    {
        readonly IMarketService marketService;

        string personalstaking = "0.00";
        public string Personalstaking
        {
            get => personalstaking;
            set
            {
                personalstaking = value;
                OnPropertyChanged();
            }

        }

        string totalcommunitystaking = "0.00";
        public string Totalcommunitystaking
        {
            get => totalcommunitystaking;
            set
            {
                totalcommunitystaking = value;
                OnPropertyChanged();
            }

        }

        string username = AppSettings.UserName;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }

        }

        public DashboardViewModel(IMarketService marketService)
        {
            this.marketService = marketService;
        }

        public override Task InitializeAsync(object navigationData) => InitialSetup();

        async Task InitialSetup()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await marketService.StakingDetailsAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            Personalstaking = response.PersonalStatcking;
            Totalcommunitystaking = response.TeamStatcking;
            Username = response.Fullname;
        }

        public ICommand GotoMasterstakingcmd => new AsyncCommand(OnMasterstaking);

        async Task OnMasterstaking() => await NavigationService.NavigateToAsync<MasterStakingViewModel>();

        public ICommand GotoCommunity => new AsyncCommand(OnCommunity);

        async Task OnCommunity() => await NavigationService.NavigateToAsync<CommunityViewModel>();


        public ICommand GotoRevenue => new AsyncCommand(OnRevenue);

        async Task OnRevenue() => await NavigationService.NavigateToAsync<RevenueViewModel>();


        public ICommand GotoStakingrecordcmd => new AsyncCommand(OnStakingrecord);

        async Task OnStakingrecord() =>await NavigationService.NavigateToAsync<StakingRecordViewModel>();

        public ICommand GotoMasternodescmd => new AsyncCommand(OnMasternodes);

        async Task OnMasternodes() {ToastService.ShowToast("Coming soon"); await Task.CompletedTask;}

        public ICommand GotoCandycmd => new AsyncCommand(OnCandy);

        async Task OnCandy() {ToastService.ShowToast("Coming soon"); await Task.CompletedTask;}


        public ICommand GotoMiningcmd => new AsyncCommand(OnMining);

        async Task OnMining(){ToastService.ShowToast("Coming soon"); await Task.CompletedTask;}

        public ICommand GotoSafetycmd => new AsyncCommand(OnSafety);

        async Task OnSafety() {ToastService.ShowToast("Coming soon"); await Task.CompletedTask;}

        public ICommand GotoNewsfeedcmd => new AsyncCommand(OnNewsfeed);

        async Task OnNewsfeed() {ToastService.ShowToast("Coming soon"); await Task.CompletedTask;}


    }
}
