﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.DTOs;
using SmartWallet.Services.Market;

namespace SmartWallet.Module.PersonalStaking.ViewModel
{
    public class PersonalStakingViewModel:ViewModelBase
    {
        readonly IMarketService marketService;

        string amount;
        public string Amount
        {
            get => amount;
            set
            {
                amount = value;
                OnPropertyChanged();
            }
        }


        string date = "2019-07-26";
        public string Date
        {
            get => date;
            set
            {
                date = value;
                OnPropertyChanged();
            }
        }

        string amountValue="0.26";
        public string AmountValue
        {
            get => amountValue;
            set
            {
                amountValue = value;
                OnPropertyChanged();
            }
        }
        bool canShowEmpty;
        public bool CanShowEmpty
        {
            get => canShowEmpty;
            set
            {
                canShowEmpty = value;
                OnPropertyChanged();
            }
        }
        List<RevenueDetail> stakingHistory;
        public List<RevenueDetail> StakingHistory
        {
            get => stakingHistory;
            set
            {
                stakingHistory = value;
                OnPropertyChanged();
            }
        }
        
        public PersonalStakingViewModel(IMarketService marketService)
        {
            this.marketService = marketService;
        }
        public override Task InitializeAsync(object navigationData) => InitialSetup(navigationData);



        async Task InitialSetup(object data)
        {
            await RevenueDetails();

            if (data != null)
            {

                Amount = data.ToString();
               

            }
            await Task.CompletedTask;
        }


        async Task RevenueDetails()
        {

            IsBusy = true;
            var response = await marketService.GetPersonalStakingAsync();
            IsBusy = false;
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            StakingHistory = response.RevenueDetails;
            Amount = response.TotalPersonalRevenue;
            CanShowEmpty = StakingHistory == null;
        }
    }
}
