﻿using SmartWallet.Module.Common;
using SmartWallet.Services.Authenticate;
using System.Windows.Input;
using SmartWallet.Utils;
using SmartWallet.Extension;
using System.Threading.Tasks;
using SmartWallet.Module.ForgotPassword.ViewModal;
using SmartWallet.Module.Register.ViewModel;
using SmartWallet.Module.Root.ViewModel;
using System;
using System.Threading;
using SmartWallet.Services.Dialog;
using Plugin.Fingerprint.Abstractions;

namespace SmartWallet.Module.Login.ViewModel
{
    public class LoginViewModel : ViewModelBase

    {
        readonly IAuthenticateService authenticateService;
        readonly IDialogService dialogServices;
        string username = AppSettings.UserName;
        public string UserName
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
                if (Verificationcode != string.Empty && Password != string.Empty)
                    CanEnableLoginButton = true;
            }
        }

        string password = string.Empty;
        public string Password
        {
            get => password;
            set
            {
                password = value;
                OnPropertyChanged();
                if (UserName != string.Empty && Verificationcode != string.Empty)
                    CanEnableLoginButton = true;
            }
        }


        string deviceId;
        public string DeviceId
        {
            get => deviceId;
            set
            {
                deviceId = value;
                OnPropertyChanged();
            }
        }


        string buttonText;
        public string ButtonText
        {
            get => buttonText;
            set
            {
                buttonText = value;
                OnPropertyChanged();
            }
        }
        string timer;
        public string Timer
        {
            get => timer;
            set
            {
                timer = value;
                OnPropertyChanged();
            }
        }
        string verificationcode = string.Empty;
        public string Verificationcode
        {
            get => verificationcode;
            set
            {
                verificationcode = value;
                OnPropertyChanged();
                if (UserName != string.Empty && Password != string.Empty)
                    CanEnableLoginButton = true;
            }
        }
        bool canEnableLoginButton;
        public bool CanEnableLoginButton
        {
            get => canEnableLoginButton;
            set
            {
                canEnableLoginButton = value;
                OnPropertyChanged();
            }
        }
        bool isPassword = true;
        public bool IsPassword
        {
            get => isPassword;
            set
            {
                isPassword = value;
                OnPropertyChanged();
            }
        }
        bool userStack = true;
        public bool UserStack
        {
            get => userStack;
            set
            {
                userStack = value;
                OnPropertyChanged();
            }
        }
        bool passwordStack;
        public bool PasswordStack
        {
            get => passwordStack;
            set
            {
                passwordStack = value;
                OnPropertyChanged();
            }
        }
        string passwordImage = "eye";
        public string PasswordImage
        {
            get => passwordImage;
            set
            {
                passwordImage = value;
                OnPropertyChanged();
            }
        }









        public ICommand Languagecmd => new AsyncCommand(OnLanguage);
        public ICommand FingerPrintCmd => new AsyncCommand(AuthenticationAsync);

        public ICommand Logincmd => new AsyncCommand(OnLogin);
        public ICommand ForgotPasswordcmd => new AsyncCommand(OnForgotPassword);
        public ICommand CreateNewWalletcmd => new AsyncCommand(OnCreateAccount);
        public ICommand ForgotUserIdCmd => new AsyncCommand(ForgotUserId);
        public ICommand ChangeRandomNumberCmd => new AsyncCommand(ChangeRandomNumber);
        public ICommand ChangePasswordTextCmd => new AsyncCommand(ChangePasswordText);
        CancellationTokenSource _cancel;
        public LoginViewModel(IAuthenticateService authenticateService, IDialogService dialogServices)
        {
            this.authenticateService = authenticateService;
            this.dialogServices = dialogServices;
        }

        public override Task InitializeAsync(object navigationData) => InitialSetup();

        async Task InitialSetup()
        {
            UserStack = !AppSettings.IsLogin;
            PasswordStack = AppSettings.IsLogin;
            await ChangeRandomNumber();
        }

        async Task ChangePasswordText()
        {
            if (IsPassword)
            {
                IsPassword = false;
                PasswordImage = "eyecross";
            }
            else
            {
                IsPassword = true;
                PasswordImage = "eye";
            }
            await Task.CompletedTask;
        }





        async Task ForgotUserId()
        {
            await NavigationService.NavigateToAsync<ForgetUserIdViewModel>();
        }
        async Task OnLogin()
        {
            if (!PasswordStack)
            {

                if (!IsValidUserName()) return;
                if (!NetworkUtils.IsConnected)
                {
                    ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                    return;
                }

                IsBusy = true;
                var response1 = await authenticateService.VerifyUseridAsync(UserName);
                IsBusy = false;
                if (!response1.IsSuccess())
                {
                    ToastService.ShowToast(response1.Message);
                    return;
                }
                UserStack = false;
                PasswordStack = true;
                return;
            }


            if (!IsValidUserName()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            var response = await authenticateService.MemberLoginAsync(UserName, Password);
            IsBusy = false;
            if (!response.IsSuccess() || string.IsNullOrEmpty(response.Token))
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            AppSettings.Token = response.Token;
            AppSettings.UserName = UserName;
            AppSettings.IsLogin = true;            
            await NavigationService.NavigateToAsync<RootViewModel>();
        }


        bool IsValidUserName()
        {
            var isValid = false;
            if (!UserName.IsValid())
                ToastService.ShowToast(Constants.EMPTY_USERNAME);
            else isValid = true;
            return isValid;
        }

        async Task OnForgotPassword() => await NavigationService.NavigateToAsync<ForgotPasswordViewModel>();

        async Task OnLanguage() => await Task.CompletedTask;

        async Task ChangeRandomNumber()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            int sum = _rdm.Next(_min, _max);
            if (ButtonText == sum.ToString())
                await ChangeRandomNumber();
            else
                ButtonText = sum.ToString();
        }


        async Task OnCreateAccount() => await NavigationService.NavigateToAsync<RegisterViewModal>();        

        async Task AuthenticationAsync()
        {
            if (!AppSettings.IsLogin)
            {
                await dialogServices.ShowAlertAsync("Next Gen", "Token has been expired or not yet created, please login using password",Constants.OK);
                return;
            }
            string cancel = null, fallback = null, reason = "Place your finger on the finger panel";
            _cancel = new CancellationTokenSource();

            var dialogConfig = new AuthenticationRequestConfiguration(reason)
            {
                CancelTitle = cancel,
                FallbackTitle = fallback,
                UseDialog = true
            };
            var fingerAvailable = await Plugin.Fingerprint.CrossFingerprint.Current.IsAvailableAsync();
            if (fingerAvailable)
            {
                var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync
                             (dialogConfig, _cancel.Token);

                if (result.Authenticated)
                {
                    await NavigationService.NavigateToAsync<RootViewModel>();
                }
                else
                {
                    await dialogServices.ShowAlertAsync("FingerPrintError", result.ErrorMessage, Constants.OK);
                }
            }
            else
            {
                await dialogServices.ShowAlertAsync("FingerPrintError", "No Registered fingers in your phone.", Constants.OK);
            }
        }
    }
}
