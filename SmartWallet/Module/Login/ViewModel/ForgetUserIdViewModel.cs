﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Module.Phrase.ViewModel;
using SmartWallet.Services.Authenticate;
using SmartWallet.Utils;

namespace SmartWallet.Module.Login.ViewModel
{
    public class ForgetUserIdViewModel : ViewModelBase
    {
        readonly IAuthenticateService authenticateService;
        bool optionsStack = true;
        public bool OptionsStack
        {
            get => optionsStack;
            set
            {
                optionsStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameEntryStack;
        public bool UserNameEntryStack
        {
            get => userNameEntryStack;
            set
            {
                userNameEntryStack = value;
                OnPropertyChanged();
            }
        }
        bool userNameStack;
        public bool UserNameStack
        {
            get => userNameStack;
            set
            {
                userNameStack = value;
                OnPropertyChanged();
            }
        }
        bool phraseStack;
        public bool PhraseStack
        {
            get => phraseStack;
            set
            {
                phraseStack = value;
                OnPropertyChanged();
            }
        }
        bool showOptions;
        public bool ShowOptions
        {
            get => showOptions;
            set
            {
                showOptions = value;
                OnPropertyChanged();
            }
        }
        bool cofirmationMessageStack;
        public bool CofirmationMessageStack
        {
            get => cofirmationMessageStack;
            set
            {
                cofirmationMessageStack = value;
                OnPropertyChanged();
            }
        }
        bool errorMessage;
        public bool ErrorMessage
        {
            get => errorMessage;
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }
        string userName;
        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged();
                ErrorMessage = false;
            }
        }
        string phraseWords;
        public string PhraseWords
        {
            get => phraseWords;
            set
            {
                phraseWords = value;
                OnPropertyChanged();
            }
        }

        public ICommand ShowOptionsCmd => new AsyncCommand(OnShowOptions);
        public ICommand ShowConfirmationCmd => new AsyncCommand(OnShowConfirmation);
        public ICommand ShowUserStackCmd => new AsyncCommand(OnShowUserStack);
        public ICommand GotoLoginCmd => new AsyncCommand(GotoLogin);
        public ICommand ResendEmailCmd => new AsyncCommand(ResendEmail);
        public ICommand ClearCmd => new AsyncCommand(OnEmpty);
        public ICommand ForgotPhraseCmd => new AsyncCommand(OnShowConfirmation);


        public ForgetUserIdViewModel(IAuthenticateService authenticateService)
        {
            this.authenticateService = authenticateService;
        }

        async Task OnShowOptions()
        {
            if (ShowOptions)
                ShowOptions = false;
            else
                ShowOptions = true;
            await Task.CompletedTask;
        }

        async Task GotoLogin()
        {
            await NavigationService.NavigateBackAsync();
        }

        async Task OnShowUserStack(object data)
        {
            if (data.ToString() == "User")
            {
                OptionsStack = false;
                UserNameEntryStack = true;
            }
            else
            {
                OptionsStack = false;
                PhraseStack = true;
            }
            await Task.CompletedTask;
        }

        async Task OnShowConfirmation()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            IsBusy = true;
            if (PhraseStack)
            {
                var response1 = await authenticateService.ForgotUserIdByMneMonicAsync(PhraseWords);
                IsBusy = false;
                if (!response1.IsSuccess())
                {
                    ToastService.ShowToast(response1.Message);
                    return;
                }
                PhraseStack = false;
                CofirmationMessageStack = true;
            }
            else
            {
                if (!UserName.IsValidMail())
                {
                    ToastService.ShowToast(Constants.INVALID_EMAIL);
                    return;
                }
                var response = await authenticateService.ForgotUserIdByEmailAsync(UserName);
                IsBusy = false;
                if (!response.IsSuccess())
                {
                    ToastService.ShowToast(response.Message);
                    return;
                }
                UserNameEntryStack = false;
                CofirmationMessageStack = true;
            }
        }

        async Task ResendEmail()
        {
            await OnShowConfirmation();
            ToastService.ShowToast("Message Resended");
        }
        async Task OnEmpty()
        {
            UserName = string.Empty;
            await Task.CompletedTask;
        }
    }
}
