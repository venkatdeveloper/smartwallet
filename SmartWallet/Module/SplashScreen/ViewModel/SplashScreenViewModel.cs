﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SmartWallet.Module.Common;
using SmartWallet.Module.Login.ViewModel;

namespace SmartWallet.Module.SplashScreen.ViewModel
{
    public class SplashScreenViewModel:ViewModelBase
    {

       


        public SplashScreenViewModel()
        {            
            
        }
        public override Task InitializeAsync(object navigationData) => GetTaskAsync();

        async Task GetTaskAsync()
        {
            await Task.Delay(2000);
            await NavigationService.NavigateToAsync<LoginViewModel>();
        }
    }
}
