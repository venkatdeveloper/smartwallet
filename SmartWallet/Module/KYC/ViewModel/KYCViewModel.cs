﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Media;
using Plugin.Media.Abstractions;
using SmartWallet.Extension;
using SmartWallet.Module.Common;
using SmartWallet.Services.Dialog;
using SmartWallet.Services.Support;
using SmartWallet.Utils;
using Xamarin.Forms;

namespace SmartWallet.Module.KYC.ViewModel
{
    public class KYCViewModel : ViewModelBase
    {
        readonly IDialogService dialogService;
        readonly ISupportService supportService;
        string backImageUrlPath;

        ImageSource cardFrontImage = "ic_camera";
        public ImageSource CardFrontImage
        {
            get => cardFrontImage;
            set
            {
                cardFrontImage = value;
                OnPropertyChanged(nameof(CardFrontImage));
            }
        }


        string username;
        public string Username
        {
            get => username;
            set
            {
                username = value;
                OnPropertyChanged();
            }

        }
        string idNum;
        public string IdNum
        {
            get => idNum;
            set
            {
                idNum = value;
                OnPropertyChanged();
            }

        }
        


        public ICommand GotoExternalstorage => new AsyncCommand(OnChooseIdBackSide);

        public ICommand UploadCmd => new AsyncCommand(OnUpload);

        public KYCViewModel(IDialogService dialogService, ISupportService supportService)
        {
            this.dialogService = dialogService;
            this.supportService = supportService;

        }
        public override Task InitializeAsync(object navigationData) => GetKyc();

        async Task GetKyc()
        {
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            var response = await supportService.GetKycAsync();
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            CardFrontImage = response.IDcardFrontUrl != null || response.IDcardFrontUrl != string.Empty || response.IDcardFrontUrl != " " ? response.IDcardFrontUrl : "ic_camera";
            Username = response.Username;
            IdNum = response.KycStatus;
        }

        async Task OnChooseIdBackSide()
        {
            var filePath = await OnExternalstorage();
            backImageUrlPath = filePath.Path;
            CardFrontImage = ImageSource.FromFile(filePath.Path);
        }

        async Task<MediaFile> OnExternalstorage()
        {

            var selectedOption = await dialogService.ShowActionSheetAsync(Constants.PHOTO_CHOOSEN_TITLE, Constants.CANCEL, null, new string[] { Constants.CAMERA, Constants.GALLERY });
            switch (selectedOption)
            {
                case Constants.GALLERY:
                    return await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                    {
                        PhotoSize = PhotoSize.Medium,
                        CompressionQuality = 80
                    });
                case Constants.CAMERA:
                    return await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                    {
                        PhotoSize = PhotoSize.Medium,
                        CompressionQuality = 80
                    });
            }
            return null;
        }


        async Task OnUpload()
        {
            if (!IsValidData()) return;
            if (!NetworkUtils.IsConnected)
            {
                ToastService.ShowToast(Constants.CHECK_CONNECTIVITY);
                return;
            }
            var response = await supportService.UpdateKycAsync(backImageUrlPath);
            if (!response.IsSuccess())
            {
                ToastService.ShowToast(response.Message);
                return;
            }
            ToastService.ShowToast(response.Message);
        }


        bool IsValidData()
        {
            bool isValid = false;
            if (!backImageUrlPath.IsValid())
                ToastService.ShowToast(Constants.EMPTY_IMAGE);
            else isValid = true;
            return isValid;
        }
    }
}

