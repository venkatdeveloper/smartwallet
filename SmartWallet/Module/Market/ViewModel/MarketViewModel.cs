﻿using System;
using System.Collections.Generic;
using SmartWallet.Module.Assets.Model;
using SmartWallet.Module.Common;
namespace SmartWallet.Module.Market.ViewModel
{
    public class MarketViewModel : ViewModelBase
    {


        List<Coins> coinsList;
        public List<Coins> CoinsList
        {
            get => coinsList;
            set
            {
                coinsList = value;
                OnPropertyChanged();
            }
        }

        


        string logo;
        public string Logo
        {
            get => logo;
            set
            {
                logo = value;
                OnPropertyChanged();
            }

        }


        public MarketViewModel()
        {
            CoinsList = new List<Coins>
           {
               new Coins
               {
                   Name="BTC",AvailableVolume="$11,420.00",Rate="+3.97%",Logo="Bitcoin"


               },
               new Coins
               {
                   Name="ETH",AvailableVolume="$12,420.00",Rate="+4.97%",Logo="Etherium"
               },
               new Coins
               {
                   Name="LTC",AvailableVolume="$17,420.00",Rate="+6.97%",Logo="ltc"

               },

                new Coins
               {
                   Name="MOD",AvailableVolume="$18,420.00",Rate="+6.97%",Logo="mod_login_logo"

               },

                 new Coins
               {
                   Name="USDT",AvailableVolume="$19,420.00",Rate="+8.97%",Logo="usdt"

               },

           };

        }
    }
}

