﻿using SmartWallet.iOS.CustomViews;
using SmartWallet.iOS.DI;
using SmartWallet.Services.Toast;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastHandler))]
namespace SmartWallet.iOS.DI
{
    public class ToastHandler : IToastService
    {
        public void ShowToast(string message)
        {
            Toast.Shared.Show(message, ToastDuration.Short);
        }
    }
}