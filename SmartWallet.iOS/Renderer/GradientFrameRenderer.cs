﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using CoreAnimation;
using UIKit;
using CoreGraphics;
using SmartWallet.Renderer;
using SmartWallet.iOS.Renderer;
using System.ComponentModel;
using SmartWallet.iOS.Renderers;

[assembly: ExportRenderer(typeof(GradientFrame), typeof(GradientFrameRenderer))]
namespace SmartWallet.iOS.Renderers
{
    public class GradientFrameRenderer : FrameRenderer
    {
        CAGradientLayer gradientLayer;
        CAShapeLayer shape;

        GradientFrame formsElement;

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (Element == null)
                return;

            formsElement = Element as GradientFrame;

            gradientLayer = new CAGradientLayer
            {
                Colors = new[] { formsElement.StartColor.ToCGColor(), formsElement.EndColor.ToCGColor() },
                StartPoint = new CGPoint(0, 1),
                EndPoint = new CGPoint(1, 0),
                Frame = new CGRect(CGPoint.Empty, this.Bounds.Size)
            };

            Layer.InsertSublayer(gradientLayer, 0);
            Layer.MasksToBounds = true;

            shape = new CAShapeLayer
            {
                LineWidth = 3,
                Path = UIBezierPath.FromRoundedRect(Bounds, formsElement.CornerRadius).CGPath,
                StrokeColor = UIColor.Black.CGColor,
                FillColor = UIColor.Clear.CGColor,
                CornerRadius = formsElement.CornerRadius
            };
            gradientLayer.Mask = shape;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == GradientFrame.IsVisibleGradientProperty.PropertyName)
            {
                shape.FillColor = formsElement.IsVisibleGradient ? UIColor.Black.CGColor : UIColor.Clear.CGColor;
            }
            else if (e.PropertyName == GradientFrame.IsBorderVisibleProprty.PropertyName)
            {
                shape.StrokeColor = formsElement.IsBorderVisible ? UIColor.Black.CGColor : UIColor.Clear.CGColor;
            }
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            shape.StrokeColor = formsElement.IsBorderVisible ? UIColor.Black.CGColor : UIColor.Clear.CGColor;
            shape.FillColor = formsElement.IsVisibleGradient ? UIColor.Black.CGColor : UIColor.Clear.CGColor;
            shape.Path = UIBezierPath.FromRoundedRect(Bounds, formsElement.CornerRadius).CGPath;
            gradientLayer.Frame = new CGRect(CGPoint.Empty, Bounds.Size);
        }
    }
}