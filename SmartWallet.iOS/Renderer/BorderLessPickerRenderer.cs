﻿using System;
using SmartWallet.iOS.Renderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Picker), typeof(BorderLessPickerRenderer))]
namespace SmartWallet.iOS.Renderer
{
    public class BorderLessPickerRenderer : PickerRenderer
    {
        public BorderLessPickerRenderer()
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;
            Control.BorderStyle = UIKit.UITextBorderStyle.None;
        }
    }
}
